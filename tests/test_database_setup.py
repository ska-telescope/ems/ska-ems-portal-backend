"""
Database setup for tests
"""

from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import StaticPool

from ska_ems_portal_backend.db.database import Base
from ska_ems_portal_backend.db.session import get_db
from ska_ems_portal_backend.main import app
from ska_ems_portal_backend.utils.auth import validate_token

SQLALCHEMY_DATABASE_URL = "sqlite:///:memory:"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL,
    connect_args={"check_same_thread": False},
    poolclass=StaticPool,
)
TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


def mock_required_permissions():
    """
    Mock function to replace the permissions dependency
    """
    return {"groups": ["ems-portal-viewer"]}


Base.metadata.create_all(bind=engine)


def override_get_db():
    """
    Override the get_db function for testing.
    """
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


app.dependency_overrides[get_db] = override_get_db

client = TestClient(app)


def setup_method():
    """
    Setup method for tests.
    """
    Base.metadata.create_all(bind=engine)
    client.app.dependency_overrides[validate_token] = mock_required_permissions


def teardown_method():
    """
    Teardown method for tests.
    """
    Base.metadata.drop_all(bind=engine)
