"""
Tests the handle_request_exceptions decorator.
"""

import logging

import pytest
import requests
from fastapi import HTTPException
from sqlalchemy.exc import MultipleResultsFound, NoResultFound

from ska_ems_portal_backend.utils.custom_exceptions import (
    BadRequestException,
    ConflictException,
    ForbiddenException,
    InternalServerErrorException,
    MethodNotAllowedException,
    NotFoundException,
    ServiceUnavailableException,
    UnauthorizedException,
)
from ska_ems_portal_backend.utils.exception_handler import (
    get_http_exception,
    handle_request_exceptions,
    is_custom_http_exception,
)

logging.basicConfig(level=logging.ERROR)


class CustomTestException(Exception):
    """
    Test custom exception for testing purposes.
    """


@pytest.mark.asyncio
async def test_handle_request_exceptions_request_exception():
    """
    Test that the handle_request_exceptions decorator handles general
    RequestException.
    """

    async def mock_func():
        raise requests.exceptions.RequestException("Request exception occurred")

    decorated_func = handle_request_exceptions(mock_func)

    with pytest.raises(InternalServerErrorException) as exc_info:
        await decorated_func()

    assert exc_info.value.status_code == 500
    assert exc_info.value.detail == "Request exception"


@pytest.mark.asyncio
async def test_handle_request_exceptions_timeout():
    """
    Test that the handle_request_exceptions decorator handles Timeout exception.
    """

    async def mock_func():
        raise requests.exceptions.Timeout("Timeout exception occurred")

    decorated_func = handle_request_exceptions(mock_func)

    with pytest.raises(ServiceUnavailableException) as exc_info:
        await decorated_func()

    assert exc_info.value.status_code == 503
    assert exc_info.value.detail == "Timeout exception occurred"


@pytest.mark.asyncio
async def test_handle_request_exceptions_connection_error():
    """
    Test that the handle_request_exceptions decorator handles ConnectionError exception.
    """

    async def mock_func():
        raise requests.exceptions.ConnectionError("Connection error occurred")

    decorated_func = handle_request_exceptions(mock_func)

    with pytest.raises(ServiceUnavailableException) as exc_info:
        await decorated_func()

    assert exc_info.value.status_code == 503
    assert exc_info.value.detail == "Connection error occurred"


@pytest.mark.asyncio
async def test_handle_request_exceptions_http_error():
    """
    Test that the handle_request_exceptions decorator handles HTTPError exception.
    """

    async def mock_func():
        response = requests.models.Response()
        response.status_code = 404
        raise requests.exceptions.HTTPError("HTTP error occurred", response=response)

    decorated_func = handle_request_exceptions(mock_func)

    with pytest.raises(NotFoundException) as exc_info:
        await decorated_func()

    assert exc_info.value.status_code == 404
    assert exc_info.value.detail == "Not Found"


@pytest.mark.asyncio
async def test_handle_request_exceptions_key_error():
    """
    Test that the handle_request_exceptions decorator handles KeyError exception.
    """

    async def mock_func():
        raise KeyError("Key error occurred")

    decorated_func = handle_request_exceptions(mock_func)

    with pytest.raises(BadRequestException) as exc_info:
        await decorated_func()

    assert exc_info.value.status_code == 400
    assert exc_info.value.detail == "'Key error occurred'"


@pytest.mark.asyncio
async def test_handle_request_exceptions_value_error():
    """
    Test that the handle_request_exceptions decorator handles ValueError exception.
    """

    async def mock_func():
        raise ValueError("Value error occurred")

    decorated_func = handle_request_exceptions(mock_func)

    with pytest.raises(BadRequestException) as exc_info:
        await decorated_func()

    assert exc_info.value.status_code == 400
    assert exc_info.value.detail == "Value error occurred"


@pytest.mark.asyncio
async def test_handle_request_exceptions_no_result_found():
    """
    Test that the handle_request_exceptions decorator handles NoResultFound exception.
    """

    async def mock_func():
        raise NoResultFound("No result found")

    decorated_func = handle_request_exceptions(mock_func)

    with pytest.raises(NotFoundException) as exc_info:
        await decorated_func()

    assert exc_info.value.status_code == 404
    assert exc_info.value.detail == "Item not found"


@pytest.mark.asyncio
async def test_handle_request_exceptions_multiple_results_found():
    """
    Test that the handle_request_exceptions decorator
    handles MultipleResultsFound exception.
    """

    async def mock_func():
        raise MultipleResultsFound("Multiple results found")

    decorated_func = handle_request_exceptions(mock_func)

    with pytest.raises(ConflictException) as exc_info:
        await decorated_func()

    assert exc_info.value.status_code == 409
    assert exc_info.value.detail == "Multiple items found"


@pytest.mark.asyncio
async def test_handle_request_exceptions_custom_http_exception():
    """
    Test that the handle_request_exceptions decorator handles custom HTTP exceptions.
    """

    async def mock_func():
        raise BadRequestException(detail="Custom bad request")

    decorated_func = handle_request_exceptions(mock_func)

    with pytest.raises(BadRequestException) as exc_info:
        await decorated_func()

    assert exc_info.value.status_code == 400
    assert exc_info.value.detail == "Custom bad request"


@pytest.mark.asyncio
async def test_handle_request_exceptions_unexpected_error():
    """
    Test that the handle_request_exceptions decorator handles unexpected exceptions.
    """

    async def mock_func():
        raise CustomTestException("Unexpected error occurred")

    decorated_func = handle_request_exceptions(mock_func)

    with pytest.raises(InternalServerErrorException) as exc_info:
        await decorated_func()

    assert exc_info.value.status_code == 500
    assert exc_info.value.detail == "An unexpected error occurred"


@pytest.mark.asyncio
async def test_get_http_exception():
    """
    Test that the get_http_exception function returns the correct
    exception based on status code.
    """
    assert isinstance(get_http_exception(400), BadRequestException)
    assert isinstance(get_http_exception(401), UnauthorizedException)
    assert isinstance(get_http_exception(403), ForbiddenException)
    assert isinstance(get_http_exception(404), NotFoundException)
    assert isinstance(get_http_exception(405), MethodNotAllowedException)
    assert isinstance(get_http_exception(409), ConflictException)
    assert isinstance(get_http_exception(500), InternalServerErrorException)
    assert isinstance(get_http_exception(503), ServiceUnavailableException)
    assert isinstance(
        get_http_exception(418), HTTPException
    )  # Example for an unmapped status code


def test_is_custom_http_exception():
    """
    Test that the is_custom_http_exception function identifies custom HTTP exceptions.
    """
    assert is_custom_http_exception(BadRequestException(detail="Bad Request"))
    assert is_custom_http_exception(UnauthorizedException(detail="Unauthorized"))
    assert is_custom_http_exception(ForbiddenException(detail="Forbidden"))
    assert is_custom_http_exception(NotFoundException(detail="Not Found"))
    assert is_custom_http_exception(
        MethodNotAllowedException(detail="Method Not Allowed")
    )
    assert is_custom_http_exception(ConflictException(detail="Conflict"))
    assert is_custom_http_exception(
        InternalServerErrorException(detail="Internal Server Error")
    )
    assert is_custom_http_exception(
        ServiceUnavailableException(detail="Service Unavailable")
    )
    assert not is_custom_http_exception(CustomTestException("General exception"))
