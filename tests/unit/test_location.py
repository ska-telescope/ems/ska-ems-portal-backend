"""
Test location endpoints.
"""

from unittest.mock import patch

import pytest

from ska_ems_portal_backend.models.cmms import Asset, Location, Region
from tests.test_database_setup import client


@pytest.mark.asyncio
async def test_get_all_locations(db_session):
    """
    Test that the /locations endpoint returns the complete location structure.
    """
    location1 = Location(id=1, name="Location 1", region_id=2)
    location2 = Location(id=2, name="Location 2", region_id=2)
    region1 = Region(id=1, name="Region 1", parent_id=0)
    region2 = Region(id=2, name="Region 2", parent_id=1)
    region3 = Region(id=3, name="Region 3", parent_id=1)
    region4 = Region(id=4, name="Region 4", parent_id=0)

    db_session.add(location1)
    db_session.add(location2)
    db_session.add(region1)
    db_session.add(region2)
    db_session.add(region3)
    db_session.add(region4)

    db_session.commit()

    response = client.get("/locations")
    assert response.json() == [
        {
            "children": [
                {
                    "id": 2,
                    "label": "Region 2",
                    "locations": [
                        {"id": 1, "label": "Location 1", "type": 2},
                        {"id": 2, "label": "Location 2", "type": 2},
                    ],
                    "type": 1,
                },
                {"id": 3, "label": "Region 3", "locations": [], "type": 1},
            ],
            "id": 1,
            "label": "Region 1",
            "type": 1,
        },
        {"children": [], "id": 4, "label": "Region 4", "type": 1},
    ]
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_export_location_assets_success(db_session):
    """
    Test that the /locations/{location_id}/export_assets endpoint exports the assets
    as CSV correctly.
    """
    asset1 = Asset(
        id=1,
        name="Dish MID SKA001",
        part_number="PN-000001",
        version="01.00",
        serial_number="SKA001",
        topological_name="ZA+COR+SKA001",
        location_id=100,
        parent_id=0,
        status="Installed",
    )
    asset2 = Asset(
        id=2,
        name="Dish MID SKA002",
        part_number="PN-000002",
        version="01.00",
        serial_number="SKA002",
        topological_name="ZA+COR+SKA002",
        location_id=100,
        parent_id=0,
        status="Installed",
    )
    db_session.add_all([asset1, asset2])
    db_session.commit()

    location_id = 100
    response = client.get(f"/locations/{location_id}/export_assets")

    assert response.status_code == 200
    assert response.headers["Content-Type"] == "application/octet-stream"
    assert "attachment; filename=" in response.headers["Content-Disposition"]

    # Validate CSV content
    csv_lines = response.content.decode("utf-8").splitlines()
    expected_headers = [
        "id",
        "name",
        "part_number",
        "version",
        "serial_number",
        "topological_name",
        "status",
        "location_id",
        "parent_id",
    ]
    expected_row1 = [
        "1",
        "Dish MID SKA001",
        "PN-000001",
        "01.00",
        "SKA001",
        "ZA+COR+SKA001",
        "Installed",
        "100",
        "0",
    ]
    expected_row2 = [
        "2",
        "Dish MID SKA002",
        "PN-000002",
        "01.00",
        "SKA002",
        "ZA+COR+SKA002",
        "Installed",
        "100",
        "0",
    ]
    assert csv_lines[0].split(",") == expected_headers
    assert csv_lines[1].split(",") == expected_row1
    assert csv_lines[2].split(",") == expected_row2


@pytest.mark.asyncio
async def test_export_location_assets_no_assets():
    """
    Test that the /locations/{location_id}/export_assets endpoint returns 404 when no
    assets are found.
    """
    location_id = 200
    response = client.get(f"/locations/{location_id}/export_assets")

    assert response.status_code == 404
    assert response.json() == {"detail": "No assets found for the given location"}


@pytest.mark.asyncio
@patch("ska_ems_portal_backend.controllers.location.write_csv")
async def test_export_location_assets_csv_error(mock_write_csv, db_session):
    """
    Test that the /locations/{location_id}/export_assets endpoint returns 500 on CSV
    generation error.
    """
    asset = Asset(
        id=1,
        name="Dish MID SKA001",
        part_number="PN-000001",
        version="01.00",
        serial_number="SKA001",
        topological_name="ZA+COR+SKA001",
        location_id=100,
        parent_id=0,
        status="Installed",
    )
    db_session.add(asset)
    db_session.commit()

    mock_write_csv.side_effect = Exception("CSV generation failed")

    location_id = 100
    response = client.get(f"/locations/{location_id}/export_assets")

    assert response.status_code == 500
    assert response.json() == {"detail": "Error generating CSV: CSV generation failed"}


@pytest.mark.asyncio
@patch("ska_ems_portal_backend.controllers.location.AssetExportResponse.model_dump")
async def test_export_location_assets_processing_error(mock_model_dump, db_session):
    """
    Test that the /locations/{location_id}/export_assets endpoint returns 500 when
    map_assets_to_responses throws an exception.
    """
    asset = Asset(
        id=1,
        name="Dish MID SKA001",
        part_number="PN-000001",
        version="01.00",
        serial_number="SKA001",
        topological_name="ZA+COR+SKA001",
        location_id=100,
        parent_id=0,
        status="Installed",
    )
    db_session.add(asset)
    db_session.commit()

    mock_model_dump.side_effect = Exception("Forced error from model_dump")

    location_id = 100
    response = client.get(f"/locations/{location_id}/export_assets")

    assert response.status_code == 500
    assert response.json() == {
        "detail": "Error processing assets: Forced error from model_dump"
    }


@pytest.mark.asyncio
async def test_retrieve_location_assets_success(db_session):
    """
    Test that the /locations/{location_id}/assets endpoint retrieves assets
    successfully.
    """
    asset1 = Asset(
        id=1,
        name="Dish MID SKA001",
        part_number="PN-000001",
        version="01.00",
        serial_number="SKA001",
        topological_name="ZA+COR+SKA001",
        location_id=100,
        parent_id=0,
        status="Installed",
    )
    asset2 = Asset(
        id=2,
        name="Dish MID SKA002",
        part_number="PN-000002",
        version="01.00",
        serial_number="SKA002",
        topological_name="ZA+COR+SKA002",
        location_id=100,
        parent_id=1,
        status="Installed",
    )
    db_session.add_all([asset1, asset2])
    db_session.commit()

    location_id = 100
    request_data = {
        "groupKeys": [0],
        "sortModel": [{"sort": "asc", "colId": "name"}],
        "startRow": 0,
        "endRow": 10,
    }
    response = client.post(f"/locations/{location_id}/assets", json=request_data)

    assert response.status_code == 200
    assert response.json() == [
        {
            "id": "1",
            "name": "Dish MID SKA001",
            "part_number": "PN-000001",
            "version": "01.00",
            "serial_number": "SKA001",
            "topological_name": "ZA+COR+SKA001",
            "type": 1,
            "status": "Installed",
            "has_children": True,
        }
    ]


@pytest.mark.asyncio
async def test_retrieve_location_assets_no_assets():
    """
    Test that the /locations/{location_id}/assets endpoint returns an empty list when no
    assets are found.
    """
    location_id = 200
    request_data = {
        "groupKeys": [0],
        "sortModel": [{"sort": "asc", "colId": "name"}],
        "startRow": 0,
        "endRow": 10,
    }
    response = client.post(f"/locations/{location_id}/assets", json=request_data)

    assert response.status_code == 200
    assert response.json() == []
