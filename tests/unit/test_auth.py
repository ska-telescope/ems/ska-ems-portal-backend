"""
Test the Authorization service
"""

from unittest.mock import patch

from fastapi import HTTPException
from fastapi.security import HTTPAuthorizationCredentials

from ska_ems_portal_backend.utils.auth import validate_token


@patch("ska_ems_portal_backend.utils.auth.get_user_groups")
def test_valid_token(mock_get_user_groups):
    """
    Test valid token validation
    """
    mock_get_user_groups.return_value = {
        "id1": "low-ems-portal-viewer",
        "id2": "mid-ems-portal-viewer",
    }

    # Create a mock credentials object
    credentials = HTTPAuthorizationCredentials(
        scheme="Bearer", credentials="valid_token"
    )

    # Call the function under test
    result = validate_token(credentials)

    # Assert that the result contains the expected groups
    assert result == {"groups": {"low-ems-portal-viewer", "mid-ems-portal-viewer"}}


@patch("ska_ems_portal_backend.utils.auth.get_user_groups")
def test_invalid_token(mock_get_user_groups):
    """
    Test valid token validation
    """
    mock_get_user_groups.return_value = {
        "id1": "group1",
        "id2": "group2",
    }

    # Create a mock credentials object
    credentials = HTTPAuthorizationCredentials(
        scheme="Bearer", credentials="valid_token"
    )

    # Call the function under test
    try:
        validate_token(credentials)
        assert False, "Expected HTTPException was not raised"
    except HTTPException as e:
        assert e.status_code == 403
        assert e.detail == "Unauthorized: User does not belong to required group."
