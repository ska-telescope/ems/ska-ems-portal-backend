"""
Test assets endpoints.
"""

import pytest

from ska_ems_portal_backend.models.cmms import Asset, Task
from ska_ems_portal_backend.models.configuration_control import Item
from ska_ems_portal_backend.models.csdb import DataModule
from ska_ems_portal_backend.utils.enums import Datasource
from tests.test_database_setup import client


@pytest.mark.asyncio
async def test_get_work_orders(db_session):
    """
    Test that the /assets/{part_number}/work-orders endpoint returns the work orders
    correctly.
    """

    asset = Asset(
        id=1,
        name="Asset 1",
        part_number="PN1",
        version="V1",
        serial_number="SN1",
        topological_name="TN1",
        location_id=100,
        parent_id=1,
    )
    db_session.add(asset)
    db_session.commit()

    task1 = Task(
        name="Task 2",
        description="Description of Task 2",
        created_date=1622476800,
        last_edited=1625068800,
        location_id=2,
        asset_id=1,
        status=1,
        priority=1,
        due_date=1625068800,
        assigned_to="User 2",
        datasource=Datasource.LIMBLE,
        datasource_id=1,
    )
    db_session.add(task1)
    db_session.commit()

    task2 = Task(
        name="Task 3",
        description="Description of Task 3",
        created_date=1622476800,
        last_edited=1625068800,
        location_id=2,
        asset_id=1,
        status=0,
        priority=1,
        due_date=1625068800,
        assigned_to="User 2",
        datasource=Datasource.LIMBLE,
        datasource_id=2,
    )

    db_session.add(task2)
    db_session.commit()

    asset_id = 1
    task1_uuid = str(task1.id)
    task2_uuid = str(task2.id)

    response = client.get(
        f"/assets/{asset_id}/work-orders",
    )
    assert response.json() == [
        {
            "id": task1_uuid,
            "name": "Task 2",
            "description": "Description of Task 2",
            "status": 1,
            "priority": 1,
            "due_date": 1625068800,
            "assigned_to": "User 2",
        },
        {
            "id": task2_uuid,
            "name": "Task 3",
            "description": "Description of Task 3",
            "status": 0,
            "priority": 1,
            "due_date": 1625068800,
            "assigned_to": "User 2",
        },
    ]

    assert response.status_code == 200


@pytest.mark.asyncio
async def test_get_data_modules(db_session):
    """
    Test that the /assets/{part_number}/data-modules endpoint returns the
    data modules correctly.
    """
    asset = Asset(
        id=1,
        name="Asset 1",
        part_number="PN1",
        version="V1",
        serial_number="SN1",
        topological_name="TN1",
        location_id=100,
        parent_id=1,
    )
    db_session.add(asset)
    db_session.commit()

    data_module_1 = DataModule(
        id=1,
        data_module_code="DM1",
        description="Description of DM1",
        part_number="PN1",
        url="https://www.test.com/",
    )
    db_session.add(data_module_1)
    db_session.commit()

    data_module_2 = DataModule(
        id=2,
        data_module_code="DM2",
        description="Description of DM2",
        part_number="PN1",
        url="https://www.test.com/",
    )
    db_session.add(data_module_2)
    db_session.commit()

    asset_id = 1

    response = client.get(f"/assets/{asset_id}/data-modules")
    assert response.json() == [
        {
            "data_module_code": "DM1",
            "description": "Description of DM1",
            "id": 1,
            "url": "https://www.test.com/",
        },
        {
            "data_module_code": "DM2",
            "description": "Description of DM2",
            "id": 2,
            "url": "https://www.test.com/",
        },
    ]

    assert response.status_code == 200


@pytest.mark.asyncio
async def test_get_asset_detail(db_session):
    """
    Test that the /assets/{asset_id} endpoint returns the asset information
    correctly.
    """
    asset = Asset(
        id=1,
        name="Asset 1",
        part_number="PN1",
        version="V1",
        serial_number="SN1",
        topological_name="TN1",
        location_id=100,
        parent_id=1,
    )
    db_session.add(asset)
    db_session.commit()

    item1 = Item(
        id=1,
        item_number="PN1",
        version="1.0",
        description="First item",
        level=1,
        parent_id=None,
        config_item="Config001",
        seq_number="Seq001",
        uom="Unit",
        status="Active",
        quantity_per="1",
        item_class="ClassA",
        software="Software1",
        serialised=True,
        under_change=False,
        main_equipment="Equipment1",
    )
    item2 = Item(
        id=2,
        item_number="PN2",
        version="1.1",
        description="Second item",
        level=2,
        parent_id=1,
        config_item="Config002",
        seq_number="Seq002",
        uom="Unit",
        status="Inactive",
        quantity_per="2",
        item_class="ClassB",
        software="Software2",
        serialised=False,
        under_change=True,
        main_equipment="Equipment2",
    )

    db_session.add(item1)
    db_session.add(item2)
    db_session.commit()

    asset_id = 1

    response = client.get(
        f"/assets/{asset_id}",
    )
    assert response.json() == {
        "config_item": "Config001",
        "description": "First item",
        "item_class": "ClassA",
        "item_number": "PN1",
        "level": 1,
        "main_equipment": "Equipment1",
        "quantity_per": "1",
        "seq_number": "Seq001",
        "serialised": True,
        "software": "Software1",
        "status": "Active",
        "under_change": False,
        "uom": "Unit",
        "version": "1.0",
    }

    assert response.status_code == 200


@pytest.mark.asyncio
async def test_get_work_orders_totals(db_session):
    """
    Test that the /assets/{asset_id}/work-orders/totals endpoint
    """

    asset = Asset(
        id=1,
        name="Asset 1",
        part_number="PN1",
        version="V1",
        serial_number="SN1",
        topological_name="TN1",
        location_id=100,
        parent_id=1,
    )
    db_session.add(asset)
    db_session.commit()

    task1 = Task(
        name="Task 2",
        description="Description of Task 2",
        created_date=1622476800,
        last_edited=1625068800,
        location_id=2,
        asset_id=1,
        status=1,
        priority=1,
        due_date=1692100080,
        assigned_to="User 2",
    )
    db_session.add(task1)
    db_session.commit()

    task2 = Task(
        name="Task 3",
        description="Description of Task 3",
        created_date=1622476800,
        last_edited=1625068800,
        location_id=2,
        asset_id=1,
        status=0,
        priority=1,
        due_date=1692100080,
        assigned_to="User 2",
    )

    db_session.add(task2)
    db_session.commit()
    asset_id = 1

    response = client.get(
        f"/assets/{asset_id}/work-orders/totals",
    )
    assert response.status_code == 200
    assert response.json() == {
        "asset_id": 1,
        "work_orders_total": 2,
        "work_orders_per_status": {"0": 1, "1": 1},
    }
