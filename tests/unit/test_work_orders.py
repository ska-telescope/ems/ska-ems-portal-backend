"""
Test work order endpoints.
"""

import pytest

from ska_ems_portal_backend.models.cmms import Asset, Location, Region, Task
from ska_ems_portal_backend.utils.enums import Datasource
from tests.test_database_setup import client


@pytest.mark.asyncio
async def test_get_tasks_within_date_range(db_session):
    """
    Test that the /assets/tasks endpoint correctly returns tasks within the
    specified date range.
    """

    region = Region(id=1, name="Region 1")
    location = Location(id=1, name="Location 1", region=region)
    asset = Asset(id=1, name="Asset 1", part_number="PN1", location_id=1)
    db_session.add_all([region, location, asset])
    db_session.commit()

    task1 = Task(
        name="Task 1",
        description="Description of Task 1",
        created_date=1625068800,
        last_edited=1625068800,
        location_id=1,
        asset_id=1,
        status=1,
        priority=2,
        due_date=1625068800,
        assigned_to="User A",
        datasource=Datasource.LIMBLE,
        datasource_id=1,
    )

    task2 = Task(
        name="Task 2",
        description="Description of Task 2",
        created_date=169240319,
        last_edited=1692100080,
        location_id=1,
        asset_id=1,
        status=1,
        priority=1,
        due_date=1692100080,
        assigned_to="User B",
        datasource=Datasource.LIMBLE,
        datasource_id=1,
    )

    db_session.add_all([task1, task2])
    db_session.commit()

    start_date = "2023-08-10T00:00:00.000Z"
    end_date = "2023-08-18T23:59:59.999Z"

    response = client.get(
        f"/tasks?start={start_date}&end={end_date}&priority=1",
    )

    assert response.status_code == 200
    json_response = response.json()

    assert len(json_response) == 1
    assert json_response[0]["name"] == "Task 2"
    assert json_response[0]["priority"] == 1
    assert json_response[0]["status"] == 1


@pytest.mark.asyncio
async def test_get_tasks_with_priority_and_status(
    db_session,
):
    """
    Test that the /tasks endpoint filters tasks by status and priority.
    """
    region = Region(id=1, name="Region 1")
    location = Location(id=1, name="Location 1", region=region)
    asset = Asset(id=1, name="Asset 1", part_number="PN1", location_id=1)
    db_session.add_all([region, location, asset])
    db_session.commit()

    task1 = Task(
        name="Task 3",
        description="Description of Task 3",
        created_date=1692100080,  # June 1, 2021
        last_edited=1692100080,
        location_id=1,
        asset_id=1,
        status=0,
        priority=1,
        due_date=1692100080,  # August 15, 2023
        assigned_to="User A",
    )

    task2 = Task(
        name="Task 4",
        description="Description of Task 4",
        created_date=1640995200,  # January 1, 2022
        last_edited=1643587200,
        location_id=1,
        asset_id=1,
        status=1,
        priority=1,
        due_date=1692532080,  # August 20, 2023
        assigned_to="User B",
    )

    db_session.add_all([task1, task2])
    db_session.commit()

    start_date = "2023-08-10T00:00:00.000Z"
    end_date = "2023-08-18T23:59:59.999Z"

    response = client.get(
        f"/tasks?start={start_date}&end={end_date}&priority=1&status=0"
    )

    assert response.status_code == 200
    json_response = response.json()

    assert len(json_response) == 1
    assert json_response[0]["name"] == "Task 3"
    assert json_response[0]["priority"] == 1
    assert json_response[0]["status"] == 0
