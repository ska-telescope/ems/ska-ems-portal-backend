"""
Test settings endpoints.
"""

from datetime import datetime
from unittest.mock import patch

import pytest

from ska_ems_portal_backend.models.api_gateway import Token
from tests.test_database_setup import client


@pytest.mark.asyncio
async def test_generate_api_gateway_token(db_session):
    """
    Test the /generate-api-gateway-token endpoint to generate a new token.
    """
    # Mock token creation and expiration
    with patch(
        "ska_ems_portal_backend.controllers.settings.create_token",
        return_value="mock_token",
    ), patch(
        "ska_ems_portal_backend.controllers.settings.calculate_expiration",
        return_value=datetime.now(),
    ):

        token_data = {
            "client_id": "1",
            "expiration": "1 Month",
            "scope": "Read Only",
            "description": "Test token",
        }

        response = client.post(
            "/generate-api-gateway-token",
            json=token_data,
        )

        assert response.status_code == 200
        response_data = response.json()
        assert response_data["client_id"] == "1"
        assert response_data["token"] == "mock_token"
        assert response_data["scope"] == "Read Only"
        assert response_data["description"] == "Test token"

        token_in_db = db_session.query(Token).filter(Token.client_id == 1).first()
        assert token_in_db is not None
        assert token_in_db.token == "mock_token"


@pytest.mark.asyncio
async def test_get_tokens_for_user(db_session):
    """
    Test the /tokens/{user_id} endpoint to retrieve all tokens for a specific user.
    """
    mock_token = Token(
        client_id="1",
        token="mock_token_123",
        expires_at=int(datetime.now().timestamp()) + 3600,
        scope="READ_WRITE",
        description="Test token",
        creation_date=int(datetime.now().timestamp()),
        last_used=None,
    )

    db_session.add(mock_token)
    db_session.commit()

    response = client.get("/tokens/1")

    assert response.status_code == 200
    response_data = response.json()

    assert len(response_data) == 1
    assert response_data[0]["scope"] == "Read & Write"
    assert response_data[0]["description"] == "Test token"


@pytest.mark.asyncio
async def test_delete_token_success(db_session):
    """
    Test that a token can be successfully deleted.
    """
    mock_token = Token(
        client_id="1",
        token="mock_token_123",
        expires_at=int(datetime.now().timestamp()) + 3600,
        scope="READ_WRITE",
        description="Test token",
        creation_date=int(datetime.now().timestamp()),
        last_used=None,
    )

    db_session.add(mock_token)
    db_session.commit()

    response_delete = client.delete("/delete-token/1")

    assert response_delete.status_code == 200
    assert response_delete.json() == {
        "detail": "Token with ID 1 has been deleted successfully."
    }
    deleted_token = db_session.query(Token).filter(Token.id == 1).first()
    assert deleted_token is None
