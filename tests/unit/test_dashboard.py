"""
Tests the dashboard endpoints.
"""

from ska_ems_portal_backend.models.cmms import Asset
from ska_ems_portal_backend.models.dashboard import Dashboard, DashboardWidget
from tests.test_database_setup import client


def test_get_dashboard_endpoint_global_dashboard_exists(db_session):
    """
    Test that the endpoint returns the global dashboard when no dashboard_id is provided
    and a global dashboard is present.
    """
    dashboard = Dashboard(
        id=1,
        title="Global Dashboard",
        is_global=True,
        owner_user_id=None,
        created_at=1728385746,
        updated_at=1728385746,
    )
    db_session.add(dashboard)
    db_session.commit()

    response = client.get("/dashboards")
    assert response.status_code == 200

    data = response.json()
    assert data["id"] == 1
    assert data["title"] == "Global Dashboard"
    assert data["is_global"] is True
    assert data["widgets"] == []


def test_get_dashboard_endpoint_global_dashboard_not_found():
    """
    Test that the endpoint returns a 404 when no dashboard_id is provided
    and no global dashboard is present.
    """
    response = client.get("/dashboards")
    assert response.status_code == 404
    assert response.json()["detail"] == "Dashboard not found."


def test_get_dashboard_endpoint_with_dashboard_id(db_session):
    """
    Test retrieving a specific dashboard by ID.
    """
    dashboard = Dashboard(
        id=2,
        title="User Dashboard",
        is_global=False,
        owner_user_id="user",
        created_at=1728385746,
        updated_at=1728385746,
    )
    db_session.add(dashboard)
    db_session.commit()

    response = client.get("/dashboards?dashboard_id=2")
    assert response.status_code == 200

    data = response.json()
    assert data["id"] == 2
    assert data["title"] == "User Dashboard"
    assert data["is_global"] is False
    assert data["owner_user_id"] == "user"
    assert data["widgets"] == []


def test_get_dashboard_endpoint_with_non_existent_dashboard_id():
    """
    Test retrieving a dashboard by non-existent ID returns 404.
    """
    response = client.get("/dashboards?dashboard_id=9999")
    assert response.status_code == 404
    assert response.json()["detail"] == "Dashboard not found."


def test_get_widget_data_endpoint_non_existent_widget():
    """
    Test that the endpoint returns a 404 if the widget doesn't exist.
    """
    response = client.get("/dashboard_widgets/9999")
    assert response.status_code == 404
    assert response.json()["detail"] == "Widget not found"


def test_get_dashboard_endpoint_with_widgets(db_session):
    """
    Test that the endpoint returns a dashboard and its associated widgets.
    """
    add_test_objects(db_session)

    response = client.get("/dashboards?dashboard_id=3")
    assert response.status_code == 200

    data = response.json()
    assert data["id"] == 3
    assert data["title"] == "Dashboard With Widgets"
    assert len(data["widgets"]) == 1

    widget_data = data["widgets"]

    assert widget_data[0]["id"] == 10
    assert widget_data[0]["widget_type"] == "list"
    assert widget_data[0]["widget_key"] == "asset_stats"
    assert "data" in widget_data[0]


def add_test_objects(db_session):
    """
    Create test objects.
    """
    dashboard = Dashboard(
        id=3,
        title="Dashboard With Widgets",
        is_global=False,
        owner_user_id="user",
        created_at=1728385746,
        updated_at=1728385746,
    )
    db_session.add(dashboard)
    db_session.commit()
    widget1 = DashboardWidget(
        id=10,
        dashboard_id=3,
        widget_type="list",
        title="All assets",
        widget_key="asset_stats",
        source_query="SELECT * FROM cmms_assets",
        created_at=1728385746,
        updated_at=1728385746,
    )
    db_session.add(widget1)
    db_session.commit()
    asset = Asset(
        id=1,
        name="Asset Widget",
        part_number="PN1",
        version="V1",
        serial_number="SN1",
        topological_name="TN1",
        location_id=200,
        parent_id=1,
    )
    db_session.add(asset)
    db_session.commit()


def test_get_widget_data_endpoint_existing_widget(db_session):
    """
    Test that the endpoint returns a widget's data if it exists.
    """
    add_test_objects(db_session)

    response = client.get("/dashboard_widgets/10")
    assert response.status_code == 200

    data = response.json()
    assert data["id"] == 10
    assert data["widget_type"] == "list"
    assert data["widget_key"] == "asset_stats"
    assert "data" in data
