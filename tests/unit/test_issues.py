"""
Test assets endpoints.
"""

from ska_ems_portal_backend.models.cmms import Asset
from ska_ems_portal_backend.models.prts import Component, Issue
from tests.test_database_setup import client


# pylint: disable=R0801
def test_read_issues_by_asset_id(db_session):
    """
    Test case for checking if issues are correctly retrieved for a given asset id.

    Asserts:
        - The status code is 200.
        - The number of issues returned is correct.
        - The jira_key of the issues matches the expected values.
    """
    asset = Asset(
        id=1,
        name="Asset 1",
        part_number="PN1",
        version="V1",
        serial_number="SN1",
        topological_name="TN1",
        location_id=100,
        parent_id=1,
    )
    db_session.add(asset)
    db_session.commit()

    issue1 = Issue(
        id=1,
        jira_key="ISSUE-001",
        summary="Issue summary 1",
        description="Description of the issue 1",
        status="New",
        resolution="Resolved",
        created_date=1728385746,
        resolution_date=1728385746,
        url="https://jira.skatelescope.org/rest/api/2/issue/226751",
    )
    component1 = Component(name="Component A", part_number="PN1", issue_id=1)
    component2 = Component(name="Component B", part_number="PN2", issue_id=1)
    issue2 = Issue(
        id=2,
        jira_key="ISSUE-002",
        summary="Issue summary 2",
        description="Description of the issue 2",
        status="New",
        resolution="Resolved",
        created_date=1728385746,
        resolution_date=1728385746,
        url="https://jira.skatelescope.org/rest/api/2/issue/226751",
    )
    component3 = Component(name="Component C", part_number="PN1", issue_id=2)
    component4 = Component(name="Component A", part_number="PN2", issue_id=2)

    db_session.add(issue1)
    db_session.add(component1)
    db_session.add(component2)
    db_session.add(issue2)
    db_session.add(component3)
    db_session.add(component4)
    db_session.commit()

    asset_id = 1
    response = client.get(f"/assets/{asset_id}/issues")
    assert response.status_code == 200
    assert len(response.json()) == 2
    assert response.json()[0]["jira_key"] == "ISSUE-001"
    assert response.json()[1]["jira_key"] == "ISSUE-002"
    assert "components" not in response.json()[0]
    assert "components" not in response.json()[1]


def test_read_issues_by_asset_id_no_issues(db_session):
    """
    Test case for checking the response when no part number is found.

    Asserts:
        - The status code is 404.
        - The error detail matches "Part number not found".
    """
    asset = Asset(
        id=1,
        name="Asset 1",
        part_number="PN1",
        version="V1",
        serial_number="SN1",
        topological_name="TN1",
        location_id=100,
        parent_id=1,
    )
    db_session.add(asset)
    db_session.commit()

    asset_id = 1
    response = client.get(f"/assets/{asset_id}/issues")
    assert response.status_code == 404
    assert response.json()["detail"] == "No problem reports found for PN1"
