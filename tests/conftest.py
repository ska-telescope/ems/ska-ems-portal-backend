"""
Pytest fixture to be used in testing
"""

import pytest

from tests.test_database_setup import (
    TestingSessionLocal,
    engine,
    setup_method,
    teardown_method,
)


@pytest.fixture(name="db_session", scope="function", autouse=True)
def fixture_db_session():
    """
    Database session fixture for tests.
    """
    connection = engine.connect()
    transaction = connection.begin()
    session = TestingSessionLocal(bind=connection)

    yield session

    session.close()
    transaction.rollback()
    connection.close()


@pytest.fixture(scope="function", autouse=True)
def setup_and_teardown():
    """
    Setup and teardown for each test.
    """
    setup_method()
    yield
    teardown_method()
