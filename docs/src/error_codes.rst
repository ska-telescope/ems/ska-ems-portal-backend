Error Codes
===========
* 400 Bad Request: The server cannot or will not process the request due to an apparent client error (e.g., malformed request syntax, invalid request message framing, or deceptive request routing).
* 401 Unauthorized: The request has not been applied because it lacks valid authentication credentials for the target resource.
* 403 Forbidden: The server understood the request but refuses to authorize it.
* 404 Not Found: The requested resource could not be found.
* 405 Method Not Allowed: A request method is not supported for the requested resource.
* 409 Conflict: The request could not be completed due to a conflict with the current state of the resource.
* 500 Internal Server Error: A generic error message, given when an unexpected condition was encountered and no more specific message is suitable.
* 503 Service Unavailable: The server is not ready to handle the request. Common causes are a server that is down for maintenance or that is overloaded.

Success Codes
=============
* 200 OK: The request has succeeded.
* 201 Created: The request has been fulfilled and resulted in a new resource being created.
* 202 Accepted: The request has been accepted for processing, but the processing has not been completed.
* 204 No Content: The server successfully processed the request, but is not returning any content.