=========================
API Gateway Tokens Feature
=========================

Overview
--------

This provides an overview of the API Gateway Token feature, which is used to manage tokens for accessing the API Gateway.
The feature allows creating, and managing tokens with specific permissions such as `Read Only` and `Read & Write`.
Validation of the api token happens in the ems_api_gateway.

Key Features
------------

- **Token Generation**: Create API tokens for users with customizable expiration dates and permission scopes.
- **Token Management**: Delete API tokens.

Endpoints
---------

**Generate API Gateway Token**

- **URL**: `/generate-api-gateway-token`
- **Method**: `POST`
- **Description**: Generates a new API token for accessing the API Gateway.
- **Request Body**:

    .. code-block:: json

        {
            "client_id": 123,
            "expiration": "1 Month",  # Must match one of the ExpirationOptions
            "scope": "Read and Write",  # Must match one of the TokenScope options
            "description": "Token for read and write access"
        }

- **Response**:

  .. code-block:: json

     {
         "client_id": 123,
         "token": "generated_token_string",
         "expires_at": "2024-10-12T12:00:00Z",
         "scope": "READ_ONLY",
         "description": "Token description",
         "creation_date": "2024-09-12T12:00:00Z",
         "last_used": null
     }

**Delete API Gateway Token**

- **URL**: `/delete-token/{token_id}`

- **Method**: `DELETE`

- **Description**: Deletes a token from the API Gateway database using the token's unique ID.

- **Parameters**:
    - `token_id` (int): The ID of the token to be deleted.

- **Responses**:
    - **200 OK**:

  The token is successfully deleted.

  .. code-block:: json

      {
          "detail": "Token with ID 123 has been deleted successfully."
      }

  - **404 Not Found**:

  The token does not exist in the database.

  .. code-block:: json

      {
          "detail": "Token not found"
      }

Allowed Options for Token Scope and Expiration
----------------------------------------------

When creating or managing tokens within the API Gateway, only specific options are allowed for the token scope and expiration date.
These options are enforced to ensure consistent access control and token management.

Token Scope Options
-------------------

The token scope determines the level of access granted to the user. Only the following values are allowed:

- **Read Only**: Grants read-only access to the API. Endpoints that require read permissions will allow tokens with this scope.
- **Read & Write**: Grants both read and write access to the API. This scope is necessary for endpoints that perform write operations.

Expiration Date Options
-----------------------

Tokens can have different expiration dates based on specific requirements.

Allowed Expiration Dates:

- **Never**: The token does not expire automatically.
- **1 Month**: The token expires one month from the creation date.
- **2 Months**: The token expires two months from the creation date.
- **6 Months**: The token expires six months from the creation date.
- **1 Year**: The token expires one year from the creation date.
- **2 Years**: The token expires two years from the creation date.

