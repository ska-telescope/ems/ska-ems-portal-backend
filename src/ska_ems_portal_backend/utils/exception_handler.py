"""
exception_handler.py

This module provides methods to handle request exceptions.
"""

import logging
from functools import wraps

import requests
from fastapi import HTTPException
from sqlalchemy.exc import MultipleResultsFound, NoResultFound

from ska_ems_portal_backend.utils.custom_exceptions import (
    BadRequestException,
    ConflictException,
    ForbiddenException,
    InternalServerErrorException,
    MethodNotAllowedException,
    NotFoundException,
    ServiceUnavailableException,
    UnauthorizedException,
)


def handle_request_exceptions(func):
    """
    Wrap a function in a try-except block to handle request exceptions.

    Args:
        func (function): The function to be wrapped by the decorator.

    Returns:
        function: The wrapped function with exception handling.

    Raises:
        Exception: Any exceptions raised by the wrapped function will be caught
        and handled within the decorator, logging and raising the error.
    """

    @wraps(func)
    async def wrapper(*args, **kwargs):
        try:
            return await func(*args, **kwargs)
        except (requests.exceptions.Timeout, requests.exceptions.ConnectionError) as e:
            logging.error("Service unavailable error: %s", e)
            raise ServiceUnavailableException(detail=str(e)) from e
        except requests.exceptions.HTTPError as e:
            logging.error("HTTP error occurred: %s", e)
            raise get_http_exception(e.response.status_code) from e
        except requests.exceptions.RequestException as e:
            logging.error("Request exception occurred: %s", e)
            raise InternalServerErrorException(detail="Request exception") from e
        except (KeyError, ValueError) as e:
            logging.error("Client error occurred: %s", e)
            raise BadRequestException(detail=str(e)) from e
        except NoResultFound as e:
            logging.error("No result found: %s", e)
            raise NotFoundException(detail="Item not found") from e
        except MultipleResultsFound as e:
            logging.error("Multiple results found: %s", e)
            raise ConflictException(detail="Multiple items found") from e
        except Exception as e:
            if is_custom_http_exception(e):
                raise e  # Re-raise custom HTTP exceptions without modifying the
                # detail string
            logging.error("Unexpected error: %s", e)
            raise InternalServerErrorException(
                detail="An unexpected error occurred"
            ) from e

    return wrapper


def get_http_exception(response_status_code):
    """
    Get the HTTP exception for the given response status code.
    """
    exception_map = {
        400: BadRequestException(detail="Bad Request"),
        401: UnauthorizedException(detail="Unauthorized"),
        403: ForbiddenException(detail="Forbidden"),
        404: NotFoundException(detail="Not Found"),
        405: MethodNotAllowedException(detail="Method Not Allowed"),
        409: ConflictException(detail="Conflict"),
        500: InternalServerErrorException(detail="Internal Server Error"),
        503: ServiceUnavailableException(detail="Service Unavailable"),
    }
    return exception_map.get(
        response_status_code,
        HTTPException(status_code=response_status_code, detail="HTTP error"),
    )


def is_custom_http_exception(e):
    """
    Verify if the exception is custom
    """
    return isinstance(e, HTTPException)
