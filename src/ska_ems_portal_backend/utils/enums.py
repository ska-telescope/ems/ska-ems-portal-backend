"""
Enum module for common data types used in the application.
"""

from enum import Enum as PyEnum


class Datasource(PyEnum):  # pylint: disable=too-few-public-methods
    """
    Datasourse Enum
    """

    LIMBLE = "Limble"
