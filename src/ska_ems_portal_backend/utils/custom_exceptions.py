"""
Custom HTTP exception used to ensure a consistent response format for all errors,
regardless of where they are raised.
"""

from fastapi import HTTPException


class BadRequestException(HTTPException):
    """
    Exception raised for a 400 Bad Request error.
    """

    def __init__(self, detail: str = "Bad Request"):
        super().__init__(status_code=400, detail=detail)


class UnauthorizedException(HTTPException):
    """
    Exception raised for a 401 Unauthorized error.
    """

    def __init__(self, detail: str = "Unauthorized"):
        super().__init__(status_code=401, detail=detail)


class ForbiddenException(HTTPException):
    """
    Exception raised for a 403 Forbidden error.
    """

    def __init__(self, detail: str = "Forbidden"):
        super().__init__(status_code=403, detail=detail)


class NotFoundException(HTTPException):
    """
    Exception raised for a 404 Not Found error.
    """

    def __init__(self, detail: str = "Not Found"):
        super().__init__(status_code=404, detail=detail)


class MethodNotAllowedException(HTTPException):
    """
    Exception raised for a 405 Method Not Allowed error.
    """

    def __init__(self, detail: str = "Method Not Allowed"):
        super().__init__(status_code=405, detail=detail)


class ConflictException(HTTPException):
    """
    Exception raised for a 409 Conflict error.
    """

    def __init__(self, detail: str = "Conflict"):
        super().__init__(status_code=409, detail=detail)


class InternalServerErrorException(HTTPException):
    """
    Exception raised for a 500 Internal Server Error.
    """

    def __init__(self, detail: str = "Internal Server Error"):
        super().__init__(status_code=500, detail=detail)


class ServiceUnavailableException(HTTPException):
    """
    Exception raised for a 503 Service Unavailable error.
    """

    def __init__(self, detail: str = "Service Unavailable"):
        super().__init__(status_code=503, detail=detail)
