"""
Date Time helper functions
"""

from datetime import datetime, timedelta, timezone
from typing import Optional

from ska_ems_portal_backend.models.api_gateway import ExpirationOptions


def calculate_expiration(expiration_option: ExpirationOptions) -> Optional[datetime]:
    """
    Calculate expiration date function
    """
    current_time = datetime.now()

    expiration_date = None
    if expiration_option == ExpirationOptions.ONE_MONTH:
        expiration_date = current_time + timedelta(days=30)
    elif expiration_option == ExpirationOptions.TWO_MONTHS:
        expiration_date = current_time + timedelta(days=60)
    elif expiration_option == ExpirationOptions.SIX_MONTHS:
        expiration_date = current_time + timedelta(days=182)  # Approx. 6 months
    elif expiration_option == ExpirationOptions.ONE_YEAR:
        expiration_date = current_time + timedelta(days=365)
    elif expiration_option == ExpirationOptions.TWO_YEARS:
        expiration_date = current_time + timedelta(days=730)
    return expiration_date


def to_epoch(dt):
    """
    Convert a timestamp to Epoch
    """
    return int(dt.timestamp()) if dt else None


def from_epoch(epoch_time: int):
    """
    Convert Epoch to a timestamp
    """
    return (
        datetime.fromtimestamp(epoch_time, tz=timezone.utc).isoformat()
        if epoch_time
        else None
    )


def epoch_to_iso_string(epoch_timestamp: int) -> str:
    """
    Convert Epoch to ISO String
    """
    return datetime.fromtimestamp(epoch_timestamp, tz=timezone.utc).isoformat()


def parse_iso_datetime(date_str: str) -> datetime:
    """
    Parse ISO datetime
    """
    try:
        return datetime.fromisoformat(date_str)
    except ValueError:
        # If it fails and contains 'Z', attempt replacement and retry parsing
        if date_str.endswith("Z"):
            date_str = date_str.replace("Z", "+00:00")
            return datetime.fromisoformat(date_str)
        # If it fails for another reason, re-raise the error
        raise
