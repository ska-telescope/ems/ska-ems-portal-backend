"""
Authentication services
"""

from fastapi import Depends, HTTPException, status
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer

from ska_ems_portal_backend.services.msentra import get_user_groups

security = HTTPBearer()


def validate_token(credentials: HTTPAuthorizationCredentials = Depends(security)):
    """
    Decode the token and validate ISSUER and AUDIENCE.
    Check if the token contains an EMS portal group.

    Args:
        credentials (HTTPAuthorizationCredentials): Bearer token provided via the
        Authorization header.

    Returns:
        groups (set): A set of group display names the user belongs to that match
        'ems-portal' or 'ems-api'.

    Raises:
        HTTPException: If the user does not belong to the required EMS groups,
                       an HTTP 403 Forbidden error is raised with an appropriate
                       message.
    """
    token = credentials.credentials
    # Fetch user groups
    all_groups = get_user_groups(token)

    # Filter groups containing 'ems-portal' or 'ems-api' in displayName
    ems_groups = {
        display_name
        for display_name in all_groups.values()
        if "ems-portal" in display_name.lower() or "ems-api" in display_name.lower()
    }

    if not ems_groups:
        # No matching groups found, unauthorized
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Unauthorized: User does not belong to required group.",
            headers={"WWW-Authenticate": "Bearer"},
        )

    # Return payload and list of matching groups
    return {
        # "payload": payload,
        "groups": ems_groups,
    }
