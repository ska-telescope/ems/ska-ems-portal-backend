"""
Sorting helper functions
"""


def sort_response(data, sort_model):
    """
    Sort the given data based on the specified column and direction.
    """
    if not sort_model:
        return data

    sort_by = sort_model[0].get("colId")
    sort_order = sort_model[0].get("sort")

    if sort_by and sort_by != "ag-Grid-AutoColumn":
        data.sort(
            key=lambda x: (getattr(x, sort_by) is None, getattr(x, sort_by) or ""),
            reverse=(sort_order == "desc"),
        )

    return data
