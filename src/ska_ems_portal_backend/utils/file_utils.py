"""
File handling helper functions
"""

import csv
import io


def write_csv(headers: list[str], data: list[dict]) -> bytes:
    """
    Generates CSV data from headers and a list of dictionaries.
    """
    csv_buffer = io.StringIO()
    writer = csv.writer(csv_buffer)
    writer.writerow(headers)
    for row_data in data:
        row = [row_data.get(field) for field in headers]
        writer.writerow(row)
    return csv_buffer.getvalue().encode("utf-8")
