"""
Api Token helper functions
"""

import secrets


def create_token() -> str:
    """
    Create token function
    """
    return secrets.token_hex(32)


def bigint_to_string(bigint_value):
    """
    Convert BigInt to String
    """
    if bigint_value is None:
        return None  # This will serialize as null in JSON
    return str(bigint_value)
