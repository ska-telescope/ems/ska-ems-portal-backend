"""
API Gateway Authentication Objects
"""

from enum import Enum as PyEnum

from sqlalchemy import BigInteger, Column
from sqlalchemy import Enum as SQLEnum
from sqlalchemy import Integer, String, Text

from ska_ems_portal_backend.db.database import Base


class TokenScope(PyEnum):  # pylint: disable=too-few-public-methods
    """
    API Gateway Token Scope Enum
    """

    READ_ONLY = "Read Only"
    READ_WRITE = "Read & Write"


class ExpirationOptions(PyEnum):  # pylint: disable=too-few-public-methods
    """
    API Gateway Token Expiration Date Options Enum
    """

    NEVER = "Never"
    ONE_MONTH = "1 Month"
    TWO_MONTHS = "2 Months"
    SIX_MONTHS = "6 Months"
    ONE_YEAR = "1 Year"
    TWO_YEARS = "2 Years"


class Token(Base):  # pylint: disable=too-few-public-methods
    """
    API Gateway Token Database Model

    Attributes:
    - id (int): Primary key for the token entry.
    - client_id (str): Identifier for the client associated with the token.
    - token (str): Unique token string used for authentication.
    - expires_at (int): Epoch time when the token expires. Can be null for
    non-expiring tokens.
    - scope (TokenScope): Scope of the token, defining access permissions.
    - description (str): Optional description of the token's purpose.
    - creation_date (int): Epoch time when the token was created.
    - last_used (int): Epoch time when the token was last used. Can be null if never
    used.

    Table Name:
        api_gateway_tokens
    """

    __tablename__ = "api_gateway_tokens"
    id = Column(Integer, primary_key=True, index=True)
    client_id = Column(String, nullable=False)
    token = Column(String(64), unique=True, nullable=False)
    expires_at = Column(BigInteger, nullable=True)
    scope = Column(SQLEnum(TokenScope, name="token_scope"), nullable=False)
    description = Column(Text, nullable=True)
    creation_date = Column(BigInteger, nullable=False)
    last_used = Column(BigInteger, nullable=True)
