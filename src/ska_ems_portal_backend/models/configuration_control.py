"""
    Configuration Control Objects
"""

from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import backref, relationship

from ska_ems_portal_backend.db.database import Base


class Item(Base):
    """
    Class representing a configuration control items.

    Attributes:
        - id (int): The primary key of the configuration control item.
        - item_number (str): The item number of the configuration control item.
        - version (str): The version of the configuration control item.
        - description (str): The description of the configuration control item.
        - level (int): The level of the configuration control item.
        - parent_id (int): The foreign key referencing the parent configuration
          control item.
        - config_item (str): The identifier of the configuration item.
        - seq_number (str): The sequence number associated with the configuration item.
        - uom (str): The unit of measure for the configuration item.
        - status (str): The current status of the configuration item.
        - quantity_per (str): The quantity per unit for the configuration item.
        - item_class (str): The class or category of the configuration item.
        - software (str): The software version or related information for the
        configuration item.
        - serialised (bool): Indicates whether the configuration item is serialized.
        - under_change (bool): Indicates whether the configuration item is currently
        under change.
        - main_equipment (str): The identifier for the main equipment associated with
        the configuration item.



    Table Name:
        configuration_control_items
    """

    __tablename__ = "configuration_control_items"

    id = Column(Integer, primary_key=True)
    item_number = Column(String(250))
    version = Column(String(250))
    description = Column(String(250))
    level = Column(Integer)
    parent_id = Column(Integer, ForeignKey("configuration_control_items.id"))
    children = relationship("Item", backref=backref("parent", remote_side=[id]))
    config_item = Column(String(250))
    seq_number = Column(String(250))
    uom = Column(String(250))
    status = Column(String(250))
    quantity_per = Column(String(250))
    item_class = Column(String(250))
    software = Column(String(250))
    serialised = Column(Boolean, unique=False, default=False)
    under_change = Column(Boolean, unique=False, default=False)
    main_equipment = Column(String(250))

    def __repr__(self):
        return f"<CMItemObject(id={self.id}, name={self.item_number})>"

    def get_full_description(self):
        """
        Returns a string with a full description of the task.
        """
        return f"Item ID: {self.id}, Name: {self.item_number}"
