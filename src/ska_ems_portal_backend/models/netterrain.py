"""
    NetTerrain Objects
"""

from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import backref, relationship

from ska_ems_portal_backend.db.database import Base


class Object(Base):
    """
    Class representing a NetTerrain object.

    Attributes:
        - id (int): The primary key of the object.
        - name (str): The name of the object.
        - part_number (str): The part number of the object.
        - serial_number (str): The serial number of the object.
        - shortcode (str): The shortcode of the object.
        - parent_id (int): The foreign key referencing the parent object.
        - children (relationship): Relationship attribute defining the children
          objects.

    Table Name:
        netterrain_objects
    """

    __tablename__ = "netterrain_objects"

    id = Column(Integer, primary_key=True)
    name = Column(String(250))
    part_number = Column(String(250))
    serial_number = Column(String(250))
    shortcode = Column(String(250))
    parent_id = Column(Integer, ForeignKey("netterrain_objects.id"))
    children = relationship(
        "netterrain_objects", backref=backref("parent", remote_side=[id])
    )

    def __repr__(self):
        return f"<NetTerrainObject(id={self.id}, name={self.name})>"

    def get_full_description(self):
        """
        Returns a string with a full description of the NetTerrain Object.
        """
        return f"Object ID: {self.id}, Name: {self.name}"
