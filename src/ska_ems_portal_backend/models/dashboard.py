"""
 Dashboards and dashboard widgets Object models
"""

from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Text
from sqlalchemy.orm import relationship

from ska_ems_portal_backend.db.database import Base


class Dashboard(Base):
    """
    Class representing a Dashboard object.

    Args:
        - id (int): The primary key of the dashboard.
        - title (str): The title of the dashboard.
        - is_global (bool): Indicates if this dashboard is the global dashboard.
        - owner_user_id (str): The user ID of the owner of the dashboard.
        - created_at (int): The timestamp when the dashboard was created.
        - updated_at (int): The timestamp when the dashboard was last updated.
        - widgets (relationship): Relationship attribute defining the associated
        widgets.

    Table Name:
        dashboards
    """

    __tablename__ = "dashboards"
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String(250), nullable=False)
    is_global = Column(Boolean, default=False, nullable=False)
    owner_user_id = Column(String(250), nullable=True)

    created_at = Column(Integer, nullable=False)
    updated_at = Column(Integer, nullable=False)
    widgets = relationship("DashboardWidget", back_populates="dashboard")

    def __repr__(self):
        """
        Returns a string representation of the Dashboard object.
        """
        return f"<Dashboard(id={self.id}, title={self.title})>"

    def get_full_description(self):
        """
        Returns a string with a full description of the dashboard.
        """
        return (
            f"Dashboard ID: {self.id}, Title: {self.title}, "
            f"Global: {self.is_global}, Owner: {self.owner_user_id}"
        )


class DashboardWidget(Base):
    """
    Class representing a DashboardWidget object.

    Args:
        - id (int): The primary key of the widget.
        - dashboard_id (int): The foreign key referencing the associated dashboard.
        - widget_type (str): The type of the widget.
        - title (str): The title of the widget.
        - widget_key (str): A unique key identifying the widget.
        - source_query (str): The query used by the widget.
        - created_at (int): The timestamp when the widget was created.
        - updated_at (int): The timestamp when the widget was last updated.
        - dashboard (relationship): Relationship attribute referencing the associated
        Dashboard object.

    Table Name:
        dashboard_widgets
    """

    __tablename__ = "dashboard_widgets"
    id = Column(Integer, primary_key=True, index=True)
    dashboard_id = Column(Integer, ForeignKey("dashboards.id"), nullable=False)
    widget_type = Column(String(250), nullable=False)
    title = Column(String(250), nullable=False)
    widget_key = Column(String(250), nullable=False)
    source_query = Column(Text, nullable=False)
    created_at = Column(Integer, nullable=False)
    updated_at = Column(Integer, nullable=False)
    dashboard = relationship("Dashboard", back_populates="widgets")

    def __repr__(self):
        """
        Returns a string representation of the DashboardWidget object.
        """
        return f"<DashboardWidget(id={self.id}, widget_key={self.widget_key})>"

    def get_full_description(self):
        """
        Returns a string with a full description of the widget.
        """
        return (
            f"Widget ID: {self.id}, Title: {self.title}, "
            f"Type: {self.widget_type}, Key: {self.widget_key}"
        )
