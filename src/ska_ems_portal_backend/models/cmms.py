"""
Maintenance Management Objects
"""

import uuid

from sqlalchemy import Column
from sqlalchemy import Enum as SQLEnum
from sqlalchemy import ForeignKey, Integer, String
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import backref, relationship

from ska_ems_portal_backend.db.database import Base
from ska_ems_portal_backend.utils.enums import Datasource


class Asset(Base):
    """
    Class representing an asset object.

    Attributes:
        - id (int): The unique identifier of the asset.
        - parent_id (int): The unique identifier of the parent asset.
        - name (str): The name of the asset.
        - version (str): The version of the asset.
        - serial_number (str): The serial number of the asset.
        - topological_name (str): The topological name of the asset.
        - status (str): The current status of the asset.
        - location_id (int): The identifier of the asset's location.
        - parent_id (int): The unique identifier of the parent asset (if any).
        - children (relationship): Relationship to child assets linked to this asset.
        - tasks (relationship): Relationship to the 'cmms_tasks' table.

    Table Name:
        cmms_assets
    """

    __tablename__ = "cmms_assets"

    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)
    part_number = Column(String(250), nullable=True)
    version = Column(String(250), nullable=True)
    serial_number = Column(String(250), nullable=True)
    topological_name = Column(String(250), nullable=True)
    status = Column(String(250), nullable=True)
    location_id = Column(Integer, nullable=True)
    parent_id = Column(Integer, ForeignKey("cmms_assets.id"), nullable=True)
    children = relationship("Asset", backref=backref("parent", remote_side=[id]))
    tasks = relationship("Task", back_populates="asset")

    def __repr__(self):
        return f"<CmmsAssetObject(id={self.id}, name={self.name})>"

    def get_full_description(self):
        """
        Returns a string with a full description of the asset.
        """
        return f"Asset ID: {self.id}, Name: {self.name}"


class Task(Base):
    """
    Class representing a task object.

    Attributes:
        - id (uuid): The primary key of the task.
        - name (str): The name of the task, limited to 250 characters.
        - description (str): A description of the task, up to 500 characters.
        - created_date (int): The timestamp when the task was created.
        - start_date (int, optional): The timestamp when the task is scheduled to start.
        - due_date (int, optional): The timestamp when the task is due.
        - completed_date (int, optional): The timestamp when the task was completed.
        - last_edited (int): The timestamp when the task was last edited.
        - priority (int, optional): The priority level of the task.
        - location_id (int): Foreign key referencing the location of the task.
        - location (relationship): Relationship to the 'cmms_locations' table.
        - asset_id (int): Foreign key referencing the asset associated with the task.
        - asset (relationship): Relationship to the 'cmms_assets' table.
        - status (int, optional): The current status of the task.
        - type (int, optional): The type/category of the task.
        - assigned_to (str, optional): The name of the person the task is assigned to,
          up to 250 characters.
        - datasource (enum, optional): Enum indicating the source of the task data.
        - datasource_id (int, optional): Identifier used to distinguish tasks across
          different data sources.

    Table Name:
        cmms_tasks
    """

    __tablename__ = "cmms_tasks"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = Column(String(250), nullable=False)
    description = Column(String(500), nullable=True)
    created_date = Column(Integer, nullable=False)
    start_date = Column(Integer, nullable=True)
    due_date = Column(Integer, nullable=True)
    completed_date = Column(Integer, nullable=True)
    last_edited = Column(Integer, nullable=False)
    priority = Column(Integer, nullable=True)

    location_id = Column(Integer, ForeignKey("cmms_locations.id"))
    location = relationship("Location", back_populates="tasks")

    asset_id = Column(Integer, ForeignKey("cmms_assets.id"))
    asset = relationship("Asset", back_populates="tasks")

    status = Column(Integer, nullable=True)
    type = Column(Integer, nullable=True)
    assigned_to = Column(String(250), nullable=True)
    datasource = Column(SQLEnum(Datasource, name="datasource"), nullable=True)
    datasource_id = Column(Integer, nullable=True)

    def __repr__(self):
        return f"<CmmsTaskObject(id={self.id}, name={self.name})>"

    def get_full_description(self):
        """
        Returns a string with a full description of the task.
        """
        return f"Task ID: {self.id}, Name: {self.name}"


class Region(Base):
    """
    Class representing a region object.

    Attributes:
        - id (int): The primary key of the region.
        - name (str): The name of the region.
        - parent_id (int): The foreign key referencing the parent region's id.
        - children (relationship): Relationship to the child regions,
          establishing a parent-child hierarchy.

    Table Name:
        cmms_regions
    """

    __tablename__ = "cmms_regions"

    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)
    parent_id = Column(Integer, ForeignKey("cmms_regions.id"), nullable=True)
    children = relationship("Region", backref=backref("parent", remote_side=[id]))
    locations = relationship("Location", back_populates="region")

    def __repr__(self):
        return f"<CmmsRegionObject(id={self.id}, name={self.name})>"

    def get_full_description(self):
        """
        Returns a string with a full description of the region.
        """
        return f"Region ID: {self.id}, Name: {self.name}"


class Location(Base):
    """
    Class representing a location object.

    Attributes:
        - id (int): The primary key of the location.
        - name (str): The name of the location.
        - region_id  (int): The foreign key referencing the region to which the
          location belongs.
        - tasks (relationship): Relationship to the tasks associated with this
          location.

    Table Name:
        cmms_locations
    """

    __tablename__ = "cmms_locations"

    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)
    region_id = Column(Integer, ForeignKey("cmms_regions.id"), nullable=False)
    region = relationship("Region", back_populates="locations")

    tasks = relationship("Task", back_populates="location")

    def __repr__(self):
        return f"<CmmsLocationObject(id={self.id}, name={self.name})>"

    def get_full_description(self):
        """
        Returns a string with a full description of the location.
        """
        return f"Location ID: {self.id}, Name: {self.name}"
