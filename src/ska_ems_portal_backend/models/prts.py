"""
    Problem Reporting and Tracking System Objects
"""

from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from ska_ems_portal_backend.db.database import Base


class Issue(Base):
    """
    Class representing a problem report.

    Attributes:
        - id (int): Primary key for the issue.
        - jira_key (str): Jira key for the issue.
        - summary (str): Summary of the issue.
        - description (str): Description of the issue.
        - status (str): Current status of the issue.
        - resolution (str): Resolution status or outcome of the issue.
        - created_date (int): Epoch time when the issue was created.
        - resolution_date (int): Epoch time when the issue was resolved. Can be null
        if unresolved.
        - url (str): URL linking to the related Jira issue or external resource.
        - components (relationship): Relationship to the 'prts_issue_components' table.


    Table Name:
        prts_issues
    """

    __tablename__ = "prts_issues"

    id = Column(Integer, primary_key=True, index=True)
    jira_key = Column(String(250))
    summary = Column(String(250))
    description = Column(String(250))
    status = Column(String(250))
    resolution = Column(String(250))
    created_date = Column(Integer)
    resolution_date = Column(Integer)
    url = Column(String(250))
    components = relationship("Component", back_populates="issue")

    def __repr__(self):
        return f"<PRTSIssueObject(id={self.id}, name={self.jira_key})>"

    def get_full_description(self):
        """
        Returns a string with a full description of the issue.
        """
        return f"Issue ID: {self.id}, Name: {self.jira_key}"


class Component(Base):
    """
    Class representing a Component object.

    Attributes:
        - name (str): The name of the component.
        - part_number (str): The part number of the component.
        - issue_id (int): The foreign key referencing the id of the related
          issue.
        - issue (Relationship): Relationship attribute defining the connection
          to the related issue object.

    Table Name:
        prts_issue_components
    """

    __tablename__ = "prts_issue_components"

    name = Column(String(250))
    part_number = Column(String(250), primary_key=True)
    issue_id = Column(Integer, ForeignKey("prts_issues.id"), primary_key=True)
    issue = relationship("Issue", back_populates="components")

    def __repr__(self):
        return f"<PRTSComponentObject(id={self.part_number}, name={self.name})>"

    def get_full_description(self):
        """
        Returns a string with a full description of the issue.
        """
        return f"Component ID: {self.part_number}, Name: {self.name}"


class WorkOrder(Base):
    """
    Class representing a work order in the PRTS.

    Attributes:
        - jira_key (str): The Jira key associated with the work order.
        - jira_link_id (int): The Jira link ID associated with the work order.
        - limble_task_id (int): The primary key for the work order.

    Table Name:
        prts_issue_work_orders
    """

    __tablename__ = "prts_issue_work_orders"

    jira_key = Column(String(250))
    jira_link_id = Column(Integer)
    limble_task_id = Column(Integer, primary_key=True)

    def __repr__(self):
        return f"<PRTSWorkOrderObject(id={self.limble_task_id})>"

    def get_full_description(self):
        """
        Returns a string with a full description of the issue.
        """
        return f"Work Order ID: {self.limble_task_id}"
