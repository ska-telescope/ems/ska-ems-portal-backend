"""
    Logistic Support Analysis Objects
"""

from sqlalchemy import Column, String

from ska_ems_portal_backend.db.database import Base


class LSAItem(Base):
    """
    Class representing a physical breakdown structure item.

    Attributes:
        - lcn (str): The unique identifier for the pbs item (primary key).
        - name (str): The name associated with the pbs item.
        - part_number (str): The part number associated with the pbs item.

    Table Name:
        lsa_pbs
    """

    __tablename__ = "lsa_pbs"

    lcn = Column(String(250), primary_key=True)
    name = Column(String(250), nullable=True)
    part_number = Column(String(250), nullable=True)

    def __repr__(self):
        return f"<PBSItemObject(id={self.lcn}, name={self.name})>"

    def get_full_description(self):
        """
        Returns a string with a full description of the physical breakdown
        structure item.
        """
        return f"LCN: {self.lcn}, Name: {self.name}"


class LSAFailureMode(Base):
    """
    Class representing the LSA Failure Modes.

    Attributes:
        - lcn (str): The unique identifier for the position of the failure
          mode.
        - description (str): Description of the failure mode.
        - local_effect (str): Local effect of the failure mode.
        - next_effect (str): Next effect of the failure mode.
        - end_effect (str): End effect of the failure mode.
        - detection_method (str): Method used for detection in the failure
          mode.

    Table Name:
        lsa_fmeca
    """

    __tablename__ = "lsa_fmeca"

    lcn = Column(String(250), primary_key=True)
    description = Column(String(250))
    local_effect = Column(String(250))
    next_effect = Column(String(250))
    end_effect = Column(String(250))
    detection_method = Column(String(250))

    def __repr__(self):
        return f"<FailureModeObject(id={self.lcn}, name={self.description})>"

    def get_full_description(self):
        """
        Returns a string with a full description of the failure mode.
        """
        return f"LCN: {self.lcn}, Name: {self.description}"


class LSATask(Base):
    """
    Class representing the LSA Tasks.

    Attributes:
        - lcn (str): The unique identifier for the position of the task.
        - description (str): The description of the task.
        - caution (str): Caution related to the task.
        - warning (str): Warning related to the task.

    Table Name:
        lsa_tasks
    """

    __tablename__ = "lsa_tasks"

    lcn = Column(String(250), primary_key=True)
    description = Column(String(500))
    caution = Column(String(250))
    warning = Column(String(250))

    def __repr__(self):
        return f"<TaskObject(id={self.lcn}, name={self.description})>"

    def get_full_description(self):
        """
        Returns a string with a full description of the task.
        """
        return f"LCN: {self.lcn}, Name: {self.description}"
