"""
    Common Source Database Objects
"""

from sqlalchemy import Column, Integer, String

from ska_ems_portal_backend.db.database import Base


class DataModule(Base):
    """
    Class representing a data module.

    Attributes:
        - id (int): Primary key for the data module.
        - data_module_code (str): Code for the data module.
        - part_number (str): Part number associated with the data module.
        - description (str): Description of the data module.
        - url (str): URL related to the data module.

    Table Name:
        csdb_data_modules
    """

    __tablename__ = "csdb_data_modules"

    id = Column(Integer, primary_key=True)
    data_module_code = Column(String(250))
    part_number = Column(String(250))
    description = Column(String(250))
    url = Column(String(250))

    def __repr__(self):
        return f"<DataModulesObject(id={self.id}, name={self.data_module_code})>"

    def get_full_description(self):
        """
        Returns a string with a full description of the data module.
        """
        return f"Item ID: {self.id}, Name: {self.data_module_code}"
