"""
Pydantic model for a configuration item.
"""

from typing import List, Optional

from pydantic import BaseModel


class ItemResponse(BaseModel):
    """
    Pydantic model representing the response schema for a configuration item.

    Attributes:
        - id (int): The unique identifier of the configuration item.
        - item_number (str): The item number for a configuration item.
        - description (str): The name of the user.
    """

    id: int
    item_number: str
    description: str

    class Config:  # pylint: disable=too-few-public-methods
        """
        Config:
            - orm_mode (bool): Enables compatibility with ORM models such as SQLAlchemy.
                            When set to True, allows the Pydantic model to accept ORM
                            instances and convert them to the Pydantic model format.
        """

        model_config = {"from_attributes": True}


class ConfigurationItemResponse(BaseModel):
    """
    Pydantic model representing the base schema for a configuration item.
    """

    item_number: Optional[str]
    version: Optional[str]
    description: Optional[str]
    level: Optional[int]
    config_item: Optional[str]
    seq_number: Optional[str]
    uom: Optional[str]
    status: Optional[str]
    quantity_per: Optional[str]
    item_class: Optional[str]
    software: Optional[str]
    serialised: Optional[bool] = False
    under_change: Optional[bool] = False
    main_equipment: Optional[str]


class Item(ConfigurationItemResponse):
    """
    Pydantic model representing the data schema for a configuration item.
    """

    id: int
    parent_id: Optional[int]
    children: List["Item"] = []

    class Config:  # pylint: disable=too-few-public-methods
        """
        Config:
            - from_attributes (bool): Enables compatibility with ORM models such as
            SQLAlchemy.
                            When set to True, allows the Pydantic model to accept ORM
                            instances and convert them to the Pydantic model format.
        """

        model_config = {"from_attributes": True}
