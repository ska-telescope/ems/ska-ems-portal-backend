"""
Pydantic model representing the response schema for an Asset.
"""

from typing import Optional

from pydantic import BaseModel


class AssetResponse(BaseModel):
    """
    Pydantic model representing an asset.

    Attributes:
        - id (str): Unique identifier of the asset.
        - name (str): Name of the asset.
        - part_number (Optional[str]): Part number associated with the asset. Can be
        null if not applicable.
        - version (Optional[str]): Version information of the asset. Can be null if
        not specified.
        - serial_number (Optional[str]): Serial number of the asset. Can be null if
        not provided.
        - topological_name (Optional[str]): Tpological name of the asset. Can be null.
        - type (Optional[int]): Type identifier of the asset. Can be null if
        unspecified.
        - status (Optional[str]): Current status of the asset (e.g., Active,
        Inactive). Can be null.
        - has_children (Optional[bool]): Indicates whether the asset has child
        assets. Can be null.
    """

    id: str
    name: str
    part_number: Optional[str] = None
    version: Optional[str] = None
    serial_number: Optional[str] = None
    topological_name: Optional[str] = None
    type: Optional[int] = None
    status: Optional[str] = None
    has_children: Optional[bool] = None


class AssetExportResponse(BaseModel):
    """
    Pydantic model representing exported asset details.
    """

    id: str
    name: str
    part_number: Optional[str] = None
    version: Optional[str] = None
    serial_number: Optional[str] = None
    topological_name: Optional[str] = None
    status: Optional[str] = None
    location_id: Optional[str] = None
    parent_id: Optional[str] = None
