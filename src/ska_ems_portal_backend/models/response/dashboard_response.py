"""
Pydantic models representing dashboards and widgets.
"""

from typing import Any, Optional

from pydantic import BaseModel


class DashboardWidgetResponse(BaseModel):
    """
    Pydantic model representing the response for a dashboard widget.

    Attributes:
        - id (int): The unique identifier for the widget.
        - widget_type (str): The category or type of the widget.
        - widget_key (str): A unique key for referencing the widget.
        - data (Optional[list[dict[str, any]]]): The data associated with the widget.
    """

    id: int
    widget_type: str
    widget_key: str
    data: Optional[list[dict[str, Any]]]


class DashboardBase(BaseModel):
    """
    Pydantic base model for a dashboard.

    Attributes:
        - id (int): The unique identifier of the dashboard.
        - title (str): The display title of the dashboard.
        - is_global (bool): A flag indicating if the dashboard is available globally.
        - owner_user_id (Optional[str]): The user ID of the dashboard owner.
    """

    id: int
    title: str
    is_global: bool
    owner_user_id: Optional[str]


class DashboardResponse(DashboardBase):
    """
    Pydantic model representing a dashboard with its associated widgets.

    Inherits from:
        - DashboardBase: The base attributes of a dashboard.

    Attributes:
        - widgets (list[DashboardWidgetResponse]): A list of widgets associated with
          the dashboard.
    """

    widgets: list[DashboardWidgetResponse]
