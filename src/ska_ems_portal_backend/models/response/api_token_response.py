"""
Pydantic model representing the response schema for an API Gateway Token.
"""

from typing import Optional

from pydantic import BaseModel

from ska_ems_portal_backend.models.api_gateway import ExpirationOptions, TokenScope


class TokenCreateRequest(BaseModel):
    """
    Pydantic model representing an API Gateway Token creation request.

    Attributes:
        - client_id (str): Identifier of the client requesting the token.
        - expiration (ExpirationOptions): Expiration settings for the token.
        - scope (TokenScope): Access scope defining the permissions granted by the
        token.
        - description (Optional[str]): Optional description of the token's purpose.
        Defaults to "No description".
    """

    client_id: str
    expiration: ExpirationOptions
    scope: TokenScope
    description: Optional[str] = "No description"


class TokenResponse(BaseModel):
    """
    Pydantic model representing an API Gateway Token response.

    Attributes:
        - id (int): Unique identifier of the token.
        - client_id (str): Identifier of the client associated with the token.
        - token (str): The actual token string used for authentication.
        - expires_at (Optional[str]): Expiration date and time. Can be null for
        non-expiring tokens.
        - scope (str): Scope of the token defining access permissions.
        - description (str): Description of the token's purpose.
        - creation_date (str): Date and time when the token was created.
        - last_used (Optional[str]): Date and time when the token was last used. Can
        be null if unused.
    """

    id: int
    client_id: str
    token: str
    expires_at: Optional[str]
    scope: str
    description: str
    creation_date: str
    last_used: Optional[str]


class TokenInfoResponse(BaseModel):
    """
    Pydantic model representing an API Gateway Token response.

    Attributes:
        - id (int): Unique identifier of the token.
        - expires_at (Optional[str]): Expiration date and time. Can be null for
        non-expiring tokens.
        - scope (str): Scope of the token defining access permissions.
        - description (str): Description of the token's purpose.
        - creation_date (str): Date and time when the token was created.
        - last_used (Optional[str]): Date and time when the token was last used. Can
        be null if unused.
    """

    id: int
    expires_at: Optional[str]
    scope: str
    description: str
    creation_date: str
    last_used: Optional[str]
