"""
Pydantic model representing the response schema for a work order.
"""

from typing import Dict, Optional
from uuid import UUID

from pydantic import BaseModel


class WorkOrderResponse(BaseModel):
    """
    Pydantic model representing the response schema for a work order.

    Attributes:
        - id (UUID): The unique identifier of the task.
        - status (int): The current status of the task.
        - priority (int): The priority level of the task.
        - due_date (str): The due date for the task in a string format.
        - assigned_to (str): The name of whom the task is assigned.
    """

    id: UUID
    name: str
    description: str
    status: Optional[int] = None
    priority: Optional[int] = None
    due_date: Optional[int] = None
    assigned_to: Optional[str] = None


class WorkOrderTotalsResponse(BaseModel):
    """
    Pydantic model representing the response schema for totals information of work
    orders for a given asset.

    Attributes:
        - asset_id (int): The unique identifier of the asset.
        - work_orders_total (int): The total vale of work orders for the given asset.
        - work_orders_per_status (Dict[int, int]): A dictionary mapping each work order
        status to its count for the given asset.
    """

    asset_id: int
    work_orders_total: int
    work_orders_per_status: Dict[int, int]


class CalendarResponseModel(BaseModel):
    """
    Pydantic model representing a task.
    """

    id: UUID
    name: str
    description: Optional[str]
    created_date: Optional[str]
    last_edited: Optional[str]
    location_name: Optional[str]
    region_name: Optional[str]
    asset_name: Optional[str]
    asset_part_number: Optional[str]
    status: Optional[int]
    priority: Optional[int]
    due_date: Optional[str]
    assigned_to: Optional[str]
