"""
Pydantic model representing the response schema for a data module
"""

from pydantic import BaseModel


class DataModuleResponse(BaseModel):
    """
    Pydantic model representing the response schema for a data module.

    Attributes:
        - id (int): The unique identifier of the module.
        - data_module_code (str): The code of the module.
        - description (str): The description of the module.
        - url (str): The URL related to the module.
    """

    id: int
    data_module_code: str
    description: str
    url: str

    class Config:  # pylint: disable=too-few-public-methods
        """
        Config:
            - from_attributes (bool): Enables compatibility with ORM models such as
            SQLAlchemy.
                            When set to True, allows the Pydantic model to accept ORM
                            instances and convert them to the Pydantic model format.
        """

        model_config = {"from_attributes": True}
