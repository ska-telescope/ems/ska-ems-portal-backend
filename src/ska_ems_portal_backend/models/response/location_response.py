"""
Pydantic models representing locations.
"""

from typing import List

from pydantic import BaseModel, Field


class LocationResponse(BaseModel):
    """
    Pydantic model representing the response for a single location or region.

    Attributes:
        - id (int): The unique identifier of the location or region.
        - name (str): The name of the location or region, aliased as 'label' in the
        response.
        - type (int): The type indicator (1 for a region, 2 for location).
    """

    id: int
    name: str = Field(..., alias="label")
    type: int

    class Config:  # pylint: disable=too-few-public-methods
        """
        Config class to enable compatibility with ORM models such as
            SQLAlchemy.
        """

        populate_by_name = True


class ParentLocationResponse(BaseModel):
    """
    Pydantic model representing the response for a single parent location
    with the associated children locations.

    Attributes:
        - id (int): The unique identifier of the location or region.
        - name (str): The name of the location or region, aliased as 'label' in the
        response.
        - type (int): The type indicator (1 for a region, 2 for location).
        - locations (List[LocationResponse]): A list of the children locations.
    """

    id: int
    name: str = Field(..., alias="label")
    type: int
    locations: List[LocationResponse] = []

    class Config:  # pylint: disable=too-few-public-methods
        """
        Config class to enable compatibility with ORM models such as
            SQLAlchemy.
        """

        populate_by_name = True


class LocationStructureResponse(BaseModel):
    """
    Pydantic model representing the response for a single region with the associated
    children regions.

    Attributes:
        - id (int): The unique identifier of the location or region.
        - name (str): The name of the location or region, aliased as 'label' in the
        response.
        - type (int): The type indicator (1 for a region, 2 for location).
        - children (List[ParentLocationResponse]): A list of the children locations.
    """

    id: int
    name: str = Field(..., alias="label")
    type: int
    children: List[ParentLocationResponse] = []

    class Config:  # pylint: disable=too-few-public-methods
        """
        Config class to enable compatibility with ORM models such as
            SQLAlchemy.
        """

        populate_by_name = True
