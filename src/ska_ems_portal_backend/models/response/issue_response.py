"""
Pydantic models representing issues.
"""

from typing import Optional

from pydantic import BaseModel


class Component(BaseModel):
    """
    Pydantic model representing a component.

    Attributes:
        - name (str): The name of the component.
        - number (str): The unique number of the component.
        - issue_id (int): The ID of the related issue.
    """

    name: str
    number: str
    issue_id: int

    class Config:  # pylint: disable=too-few-public-methods
        """
        Config class to enable compatibility with ORM models such as
            SQLAlchemy.
        """

        orm_mode: True


class ProblemReportResponse(BaseModel):
    """
    Pydantic model representing a problem report.

    Attributes:
        - id (int): The unique identifier of the issue.
        - jira_key (str): The Jira key associated with the issue.
        - summary (str): A brief summary of the issue.
        - description (str): A detailed description of the issue.
        - status (str): A status of the issue.
        - resolution (str): A resolution of the issue if available.
        - created_date (str): A created date of the issue.
        - resolution_date (str): A resolution date of the issue.
        - url (str): The JIRA url associated with the issue.
    """

    id: int
    jira_key: str
    summary: str
    description: str
    status: str
    resolution: Optional[str]
    created_date: int
    resolution_date: Optional[int]
    url: str

    class Config:  # pylint: disable=too-few-public-methods
        """
        Config class to enable compatibility with ORM models such as
            SQLAlchemy.
        """

        orm_mode: True
