"""
Generator function for database sessions.
"""

from ska_ems_portal_backend.db.database import SessionLocal


def get_db():
    """
    Get a database session.

    Returns:
        Session: A database session object.
    """
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
