"""
Dashboard and Widgets service
"""

from typing import Optional

from sqlalchemy import text
from sqlalchemy.orm import Session

from ska_ems_portal_backend.models.dashboard import Dashboard, DashboardWidget
from ska_ems_portal_backend.models.response.dashboard_response import (
    DashboardResponse,
    DashboardWidgetResponse,
)


def fetch_dashboard_by_id(dashboard_id: int, db: Session) -> Optional[Dashboard]:
    """
    Retrieve a dashboard from the database by its ID.

    Parameters:
        - db (Session): The database session used to perform the query.
        - dashboard_id (int): The unique identifier of the dashboard to retrieve.

    Returns:
        - Optional[Dashboard]: The dashboard object if found, otherwise None.
    """
    return db.query(Dashboard).filter(Dashboard.id == dashboard_id).first()


def fetch_global_dashboard(db: Session) -> Optional[Dashboard]:
    """
    Retrieve the dashboard marked as global

    Parameters:
        - db (Session): The database session used to perform the query.

    Returns:
        - Optional[Dashboard]: The dashboard object if found, otherwise None.
    """
    return db.query(Dashboard).filter(Dashboard.is_global).first()


def fetch_widget_by_id(widget_id: int, db: Session) -> Optional[DashboardWidget]:
    """
    Retrieve a dashboard widget from the database by its ID.

    Parameters:
        - widget_id (int): The unique identifier of the widget to retrieve.
        - db (Session): The database session used to perform the query.

    Returns:
        - Optional[DashboardWidget]: The widget object if found, otherwise None.
    """
    return db.query(DashboardWidget).filter(DashboardWidget.id == widget_id).first()


def build_dashboard_response(dashboard: Dashboard, db: Session) -> DashboardResponse:
    """
    Build a dashboard response object from a given dashboard model instance.

    Parameters:
        - dashboard (Dashboard): The dashboard model instance for which the response
          is being built.
        - db (Session): The database session used to execute widget queries.

    Returns:
        - DashboardResponse: A response containing the dashboard and its
          associated widgets.
    """
    widgets_responses = []
    for widget in dashboard.widgets:
        widgets_responses.append(build_widget_data_response(widget, db))

    return DashboardResponse(
        id=dashboard.id,
        title=dashboard.title,
        is_global=dashboard.is_global,
        owner_user_id=dashboard.owner_user_id,
        widgets=widgets_responses,
    )


def build_widget_data_response(
    widget: DashboardWidget, db: Session
) -> DashboardWidgetResponse:
    """
    Build a widget response object from a given dashboard widget model instance.

    Parameters:
        - widget (DashboardWidget): The widget model instance for which the response
          is being built.
        - db (Session): The database session used to execute the widget's source query.

    Returns:
        - DashboardWidgetResponse: A structured response containing the widget's data.
    """
    result = db.execute(text(widget.source_query))
    data = [dict(r) for r in result.mappings().all()]

    return DashboardWidgetResponse(
        id=widget.id,
        widget_key=widget.widget_key,
        widget_type=widget.widget_type,
        data=data,
    )
