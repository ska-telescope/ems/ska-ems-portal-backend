"""
Common Source Database service
"""

from sqlalchemy.orm import Session

from ska_ems_portal_backend.models.csdb import DataModule


def get_data_modules_by_part_number(part_number: str, db: Session):
    """
    Get data modules by its part number.

    Args:
        - part_number (str): The part number of the data module to retrieve.
        - db (Session): The database session to use for querying.

    Returns:
        DataModule: The data module object corresponding to the provided part
        number, or None if not found.
    """
    data_module = (
        db.query(DataModule).filter(DataModule.part_number == part_number).all()
    )
    return data_module
