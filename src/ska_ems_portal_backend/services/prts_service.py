"""
Parts service
"""

from sqlalchemy.orm import Session

from ska_ems_portal_backend.models.prts import Component, Issue


def get_issues_by_part_number(part_number: str, db: Session):
    """
    Get all problem reports of a given part_number.

    Args:
        - part_number (int): The unique identifier of the part number.
        - db (Session): The database session to perform the query.

    Returns:
        - List[IssueResponse]: A list of problem report objects that relate with the
        part number given.
    """
    issues = (
        db.query(Issue)
        .join(Component)
        .filter(Component.part_number == part_number)
        .all()
    )
    return issues
