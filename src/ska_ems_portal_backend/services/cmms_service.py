"""
Maintenance management service
"""

from datetime import datetime
from typing import List, Optional

from sqlalchemy import Integer, and_, cast, literal, or_
from sqlalchemy.orm import Session, aliased, joinedload
from sqlalchemy.sql import case, exists

from ska_ems_portal_backend.models.cmms import Asset, Location, Region, Task
from ska_ems_portal_backend.models.lsa import LSAItem
from ska_ems_portal_backend.models.response.work_order_response import (
    CalendarResponseModel,
    WorkOrderTotalsResponse,
)
from ska_ems_portal_backend.utils.date_utils import epoch_to_iso_string


def get_work_orders_asset_id(asset_id: int, db: Session):
    """
    Retrieve work orders associated with a specific asset_id.

    Args:
        - asset_id (int): The asset id to filter the work orders by.
        - db (Session): The database session.

    Returns:
        - list: A list of Task objects associated with the provided part
          number.
    """
    work_orders = (
        db.query(Task)
        .join(Asset)
        .filter(Asset.id == asset_id)
        .options(joinedload(Task.asset))
        .all()
    )

    return work_orders


def get_work_orders_by_part_number(part_number: str, db: Session):
    """
    Retrieve work orders associated with a specific part number.

    Args:
        - part_number (str): The part number to filter the work orders by.
        - db (Session): The database session.

    Returns:
        - list: A list of Task objects associated with the provided part
          number.
    """
    work_orders = (
        db.query(Task)
        .join(Asset)
        .filter(Asset.part_number == part_number)
        .options(joinedload(Task.asset))
        .all()
    )

    return work_orders


def get_work_orders_totals_by_part_number(
    part_number: str, db: Session
) -> Optional[WorkOrderTotalsResponse]:
    """
    Retrieve work orders totals associated with a specific part number.

    Args:
        - part_number (str): The part number to filter the work orders by.
        - db (Session): The database session.

    Returns:
        - List[WorkOrderTotalsResponse]: A list of WorkOrderTotalsResponse objects.
    """
    work_orders = get_work_orders_by_part_number(part_number, db)

    if not work_orders:
        return None

    asset_id = work_orders[0].asset_id
    work_orders_total = len(work_orders)
    work_orders_per_status = {}

    for work_order in work_orders:
        status = work_order.status
        if status not in work_orders_per_status:
            work_orders_per_status[status] = 0
        work_orders_per_status[status] += 1

    return WorkOrderTotalsResponse(
        asset_id=asset_id,
        work_orders_total=work_orders_total,
        work_orders_per_status=work_orders_per_status,
    )


def get_asset(asset_id: int, db: Session) -> Optional[Asset]:
    """
    Get the parent asset for the given asset_id

    Args:
        - asset_id (int): The unique identifier of the parent asset.
        - db (Session): The database session to perform the query.

    Returns:
        - Asset: The specific asset
    """
    parent_asset = db.query(Asset).filter(Asset.id == asset_id).one_or_none()
    return parent_asset


def get_regions(region_id: int, db: Session):
    """
    Get all children regions of a given region.

    Args:
        - region_id (int): The unique identifier of the parent region.
        - db (Session): The database session to perform the query.

    Returns:
        - List[Region]: A list of Region objects that are children of the
          specified region.
    """
    regions = db.query(Region).filter(Region.parent_id == region_id).all()
    return regions


def get_locations(region_id: int, db: Session):
    """
    Get all children locations of a given region.

    Args:
        - region_id (int): The unique identifier of the parent region.
        - db (Session): The database session to perform the query.

    Returns:
        - List[Location]: A list of Location objects that are children of the
          specified location.
    """
    locations = db.query(Location).filter(Location.region_id == region_id).all()
    return locations


def get_location_assets(location_id: int, db: Session) -> List[Asset]:
    """
    Get all assets of a given location.

    Args:
        - location_id (int): The unique identifier of location.
        - db (Session): The database session to perform the query.

    Returns:
        - List[Asset]: A list of Asset objects that are children of the
          specified location.
    """
    assets = db.query(Asset).filter(Asset.location_id == location_id).all()
    return assets


def get_location_asset_children(
    location_id: int, parent_id: int, db: Session
) -> List[Asset]:
    """
    Retrieve all assets for a specified location and parent asset ID, including an
    indicator of whether each asset has child assets.

    Args:
        - location_id (int): The unique identifier of the location.
        - parent_id (int): The unique identifier of the parent asset.
        - db (Session): The database session to perform the query.

    Returns:
        - List[Asset]: A list of Asset objects that are children of the specified asset.
    """
    children = aliased(Asset)
    pbs = aliased(LSAItem)

    query = db.query(
        Asset.id,
        Asset.name,
        Asset.parent_id,
        Asset.location_id,
        Asset.part_number,
        Asset.version,
        Asset.topological_name,
        Asset.serial_number,
        Asset.status,
        case(
            (exists().where(children.parent_id == Asset.id), literal(1)),
            (exists().where(pbs.part_number == Asset.part_number), literal(1)),
            else_=literal(0),
        ).label("has_children"),
    ).filter(Asset.location_id == location_id, Asset.parent_id == parent_id)

    assets = db.execute(query).fetchall()
    return assets


def get_tasks_within_date_range(
    db: Session,
    start_date: datetime,
    end_date: datetime,
    status: Optional[str] = None,
    priority: Optional[List[str]] = None,
) -> List[CalendarResponseModel]:
    """
    Get all assets of a given date range with optional status and priority filters.

    Args:
        - db (Session): The database session to perform the query.
        - start_date (datetime): The start date for the query.
        - end_date (datetime): The end date for the query.
        - status (Optional[str]): The status to filter for.
        - priority (Optional[List[str]]): The priority to filter for.

    Returns:
        - List[CalendarResponseModel]: A list of Task objects that are within the time
          range and the filters specified.
    """
    start_timestamp = int(start_date.timestamp())
    end_timestamp = int(end_date.timestamp())

    query = (
        db.query(Task)
        .join(Location)
        .join(Region)
        .join(Asset)
        .filter(
            and_(
                Task.created_date <= end_timestamp,
                or_(
                    Task.due_date.is_(None),
                    cast(Task.due_date, Integer) >= start_timestamp,
                ),
            )
        )
    )

    if status:
        query = query.filter(or_(Task.status == status, Task.status.is_(None)))

    if priority is None or not priority:
        query = query.filter(Task.priority.is_(None))
    else:
        query = query.filter(or_(Task.priority.in_(priority), Task.priority.is_(None)))

    tasks = query.all()
    return [
        CalendarResponseModel(
            id=task.id,
            name=task.name,
            description=task.description,
            created_date=epoch_to_iso_string(task.created_date),
            last_edited=epoch_to_iso_string(task.last_edited),
            location_name=task.location.name,
            region_name=task.location.region.name,
            asset_name=task.asset.name,
            asset_part_number=task.asset.part_number,
            status=task.status,
            priority=task.priority,
            due_date=(
                epoch_to_iso_string(int(task.due_date))
                if task.due_date is not None
                else None
            ),
            assigned_to=task.assigned_to,
        )
        for task in tasks
    ]
