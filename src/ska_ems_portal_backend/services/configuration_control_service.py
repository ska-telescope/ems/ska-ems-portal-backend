"""
Configuration control service
"""

from sqlalchemy import or_
from sqlalchemy.orm import Session

from ska_ems_portal_backend.models.configuration_control import Item


def find_item(text: str, db: Session):
    """
    Find items by search text.

    Args:
        - text (str): The text to search for.
        - db (Session): The database session to use for querying.

    Returns:
        List[Item]: A list of items that match the part number.
    """
    items = (
        db.query(Item.id, Item.item_number, Item.description)
        .filter(
            or_(
                Item.item_number.like(f"%{text}%"),
                Item.description.like(f"%{text}%"),
            )
        )
        .all()
    )

    return items


def get_item_by_part_number(part_number: str, db: Session):
    """
    Get an item by its part number from the database.

    Args:
        - part_number (str): The part number of the item to retrieve.
        - db (Session): The database session to use for querying.

    Returns:
        Item: The item object corresponding to the provided part number, or
        None if not found.
    """
    item = db.query(Item).filter(Item.item_number == part_number).first()
    return item
