"""
LSA service
"""

from typing import List, Optional

from sqlalchemy import func
from sqlalchemy.orm import Session

from ska_ems_portal_backend.models.lsa import LSAItem


def get_lcn_children(
    db: Session, part_number: Optional[str] = None, lcn: Optional[str] = None
) -> List[LSAItem]:
    """
    Get all PBS children based on either part_number or lcn.

    Args:
        - part_number (Optional[str]): The unique identifier of the part number.
        - lcn (Optional[str]): The lcn identifier.
        - db (Session): The database session to perform the query.

    Returns:
        - List[LSAItem]: The list of PBS children.
    """
    if part_number:
        lsa_item = (
            db.query(LSAItem).filter(LSAItem.part_number == part_number).one_or_none()
        )
    elif lcn:
        lsa_item = db.query(LSAItem).filter(LSAItem.lcn == lcn).one_or_none()
    else:
        return []

    if not lsa_item:
        return []

    children = (
        db.query(LSAItem)
        .filter(
            LSAItem.lcn.like(f"{lsa_item.lcn}%"),
            func.length(LSAItem.lcn) == len(lsa_item.lcn) + 2,
        )
        .order_by(LSAItem.lcn.asc())
        .all()
    )

    for child in children:
        child.has_children = (
            db.query(LSAItem)
            .filter(
                LSAItem.lcn.like(f"{child.lcn}%"),
                func.length(LSAItem.lcn) == len(child.lcn) + 2,
            )
            .first()
            is not None
        )

    return children
