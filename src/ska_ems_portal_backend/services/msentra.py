"""
MS Entra service
"""

import requests


def get_user_groups(token: str) -> dict[str, str]:
    """Checks user groups based on the provided token.

    Args:
        - token: The access token.

    Returns:
        - A dictionary of user permissions.

    Raises:
        - HTTPException: If token is invalid or unauthorized access.
    """
    group_properties = "id,displayName"
    headers = {"Authorization": f"Bearer {token}", "Content-Type": "application/json"}

    response = requests.get(
        f"https://graph.microsoft.com/v1.0/me/memberOf?$select={group_properties}",
        headers=headers,
        timeout=10,
    )

    if response.status_code == 200:
        groups = response.json().get("value", [])
        detailed_groups = {
            group.get("id"): group.get("displayName") for group in groups
        }
        return detailed_groups

    response.raise_for_status()
    return {}
