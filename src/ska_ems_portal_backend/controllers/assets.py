"""
API endpoints for retrieving asset information.
"""

from typing import List, Optional

from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from ska_ems_portal_backend.db.session import get_db
from ska_ems_portal_backend.models.response.csdb_data_modules_response import (
    DataModuleResponse,
)
from ska_ems_portal_backend.models.response.issue_response import ProblemReportResponse
from ska_ems_portal_backend.models.response.item_response import (
    ConfigurationItemResponse,
)
from ska_ems_portal_backend.models.response.work_order_response import (
    WorkOrderResponse,
    WorkOrderTotalsResponse,
)
from ska_ems_portal_backend.services.cmms_service import (
    get_asset,
    get_work_orders_asset_id,
    get_work_orders_totals_by_part_number,
)
from ska_ems_portal_backend.services.configuration_control_service import (
    get_item_by_part_number,
)
from ska_ems_portal_backend.services.csdb_service import get_data_modules_by_part_number
from ska_ems_portal_backend.services.prts_service import get_issues_by_part_number
from ska_ems_portal_backend.utils.auth import validate_token
from ska_ems_portal_backend.utils.custom_exceptions import NotFoundException
from ska_ems_portal_backend.utils.exception_handler import handle_request_exceptions

router = APIRouter()


@router.get(
    "/assets/{asset_id}", tags=["Assets"], response_model=ConfigurationItemResponse
)
@handle_request_exceptions
async def find_details(
    asset_id: int, db: Session = Depends(get_db), _: None = Depends(validate_token)
):
    """
    Retrieve asset detail information associated with a specific asset id.
    """
    asset = get_asset(asset_id, db)
    if not asset:
        raise NotFoundException(detail="Assets not found")

    item = get_item_by_part_number(asset.part_number, db)

    return item


@router.get(
    "/assets/{asset_id}/work-orders",
    tags=["Assets"],
    response_model=List[WorkOrderResponse],
)
@handle_request_exceptions
async def find_work_orders(
    asset_id: int, db: Session = Depends(get_db), _: None = Depends(validate_token)
):
    """
    Retrieve all work orders associated with a specific asset id.
    """
    asset = get_asset(asset_id, db)
    if not asset:
        raise NotFoundException(detail="Assets not found")

    work_orders = get_work_orders_asset_id(asset_id, db)

    if work_orders == []:
        raise NotFoundException(detail=f"No work orders found for {asset.part_number}")

    return work_orders


@router.get(
    "/assets/{asset_id}/work-orders/totals",
    tags=["Assets"],
    response_model=Optional[WorkOrderTotalsResponse],
)
@handle_request_exceptions
async def get_work_orders_totals(
    asset_id: int, db: Session = Depends(get_db), _: None = Depends(validate_token)
):
    """
    Retrieve the total value of work orders associated with a specific asset id.
    """
    asset = get_asset(asset_id, db)
    if not asset:
        raise NotFoundException(detail="Assets not found")

    work_orders = get_work_orders_totals_by_part_number(asset.part_number, db)

    if work_orders is None:
        raise NotFoundException(detail=f"No work orders found for {asset.part_number}")

    return work_orders


@router.get(
    "/assets/{asset_id}/data-modules",
    tags=["Assets"],
    response_model=List[DataModuleResponse],
)
@handle_request_exceptions
async def find_data_modules(asset_id: int, db: Session = Depends(get_db)):
    """
    Retrieve all data modules associated with a specific part number.
    """
    asset = get_asset(asset_id, db)
    if not asset:
        raise NotFoundException(detail="Assets not found")

    data_modules = get_data_modules_by_part_number(asset.part_number, db)

    if data_modules == []:
        raise NotFoundException(detail=f"No data modules found for {asset.part_number}")

    return data_modules


@router.get(
    "/assets/{asset_id}/issues",
    tags=["Assets"],
    response_model=List[ProblemReportResponse],
)
@handle_request_exceptions
async def find_problem_reports(asset_id: int, db: Session = Depends(get_db)):
    """
    Retrieve all the problem reports associated with a specified part number.
    """
    asset = get_asset(asset_id, db)
    if not asset:
        raise NotFoundException(detail="Assets not found")

    issues = get_issues_by_part_number(asset.part_number, db)
    if issues == []:
        raise NotFoundException(
            detail=f"No problem reports found for {asset.part_number}"
        )

    return issues
