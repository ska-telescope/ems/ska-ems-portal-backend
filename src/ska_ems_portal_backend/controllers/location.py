"""
API endpoints for retrieving location information.
"""

from datetime import datetime
from typing import List, Union

from fastapi import APIRouter, Depends, HTTPException, Request, Response
from sqlalchemy.orm import Session

from ska_ems_portal_backend.db.session import get_db
from ska_ems_portal_backend.models.response.asset_response import (
    AssetExportResponse,
    AssetResponse,
)
from ska_ems_portal_backend.models.response.location_response import (
    LocationResponse,
    LocationStructureResponse,
    ParentLocationResponse,
)
from ska_ems_portal_backend.services.cmms_service import (
    get_asset,
    get_location_asset_children,
    get_location_assets,
    get_locations,
    get_regions,
)
from ska_ems_portal_backend.services.lsa_service import get_lcn_children
from ska_ems_portal_backend.utils.auth import validate_token
from ska_ems_portal_backend.utils.exception_handler import handle_request_exceptions
from ska_ems_portal_backend.utils.file_utils import write_csv
from ska_ems_portal_backend.utils.sorting import sort_response

router = APIRouter()


@router.get(
    "/locations", tags=["Locations"], response_model=List[LocationStructureResponse]
)
@handle_request_exceptions
async def find_all_location(
    db: Session = Depends(get_db), token: dict = Depends(validate_token)
):
    """
    Retrieve all the locations and their children locations.
    """
    parent_regions = get_regions(0, db)

    user_groups = token.get("groups", [])
    if "low-ems-portal-viewer" in user_groups:
        parent_regions = [parent for parent in parent_regions if parent.id == 26]
    if "mid-ems-portal-viewer" in user_groups:
        parent_regions = [parent for parent in parent_regions if parent.id == 25]

    response = [
        LocationStructureResponse(
            id=parent.id,
            name=parent.name,
            type=1,
            children=get_child_regions(parent.id, db),
        )
        for parent in parent_regions
    ]

    # Sorting result list
    sort_model = [{"sort": "asc", "colId": "name"}]
    result_list = sort_response(response, sort_model)

    return result_list


@router.post(
    "/locations/{location_id}/assets",
    tags=["Locations"],
    response_model=Union[List[AssetResponse], AssetResponse],
)
@handle_request_exceptions
async def retrieve_location_assets(
    location_id: int,
    request: Request,
    db: Session = Depends(get_db),
    _: None = Depends(validate_token),
):
    """
    Retrieve all assets of a specified location.
    """

    parent_id, sort_model, start_row, end_row = await process_request(request)

    # Set default sort model
    sort_model = sort_model or [{"sort": "asc", "colId": "name"}]

    # If parent_id is not an integer, return only LSA items
    if not is_integer(parent_id):
        return get_lsa_lcns(parent_id, [], db)

    asset_children = get_location_asset_children(location_id, parent_id, db)

    # Sorting result list
    asset_children = sort_response(asset_children, sort_model)

    # Lazy loading - Applying start_row and end_row filtering
    if start_row is not None and end_row is not None:
        asset_children = asset_children[start_row:end_row]

    # Transform assets into response objects
    result_list = [
        AssetResponse(
            id=str(asset.id),
            name=asset.name,
            part_number=asset.part_number,
            version=asset.version,
            serial_number=asset.serial_number,
            topological_name=asset.topological_name,
            type=1,
            status=asset.status,
            has_children=asset.has_children,
        )
        for asset in asset_children
    ]

    # Append LSA items if needed
    if parent_id != 0:
        result_list.extend(get_lsa_lcns(parent_id, asset_children, db))

    return result_list


@router.get(
    "/locations/{location_id}/export_assets",
    tags=["Locations"],
    responses={
        404: {"description": "No assets found for the given location"},
        200: {"description": "Successful Response"},
    },
)
@handle_request_exceptions
async def export_location_assets(location_id: int, db: Session = Depends(get_db)):
    """
    Export all assets of a specified location to a CSV file.
    """
    assets = get_location_assets(location_id, db)
    if not assets:
        raise HTTPException(
            status_code=404, detail="No assets found for the given location"
        )

    try:
        result_list = []
        for asset in assets:
            status = asset.status if asset.status else None
            asset_response = AssetExportResponse(
                id=str(asset.id),
                name=asset.name,
                part_number=asset.part_number,
                version=asset.version,
                serial_number=asset.serial_number,
                topological_name=asset.topological_name,
                status=status,
                location_id=str(asset.location_id),
                parent_id=str(asset.parent_id),
            )

            result_list.append(asset_response.model_dump())

    except Exception as e:
        raise HTTPException(
            status_code=500, detail=f"Error processing assets: {str(e)}"
        ) from e

    try:
        headers = result_list[0].keys() if result_list else []
        csv_data = write_csv(headers, result_list)
    except Exception as e:
        raise HTTPException(
            status_code=500, detail=f"Error generating CSV: {str(e)}"
        ) from e

    current_date = datetime.now().strftime("%d-%m-%Y")
    filename = f"{location_id}_assets_{current_date}.csv"
    return Response(
        content=csv_data,
        media_type="text/csv",
        headers={
            "Content-Type": "application/octet-stream",
            "Content-Disposition": f"attachment; filename={filename}",
        },
    )


async def process_request(request: Request):
    """
    Process the request to extract parent id and sort model.
    """
    body = await request.json() if request.headers.get("content-length") != "0" else {}

    print(body)

    sort_model = body.get("sortModel", [])
    group_keys = body.get("groupKeys", [])
    start_row = body.get("startRow")
    end_row = body.get("endRow")

    parent_id = group_keys[-1] if group_keys else 0
    return parent_id, sort_model, start_row, end_row


def get_child_regions(parent_id: int, db: Session) -> List[ParentLocationResponse]:
    """
    Get all children regions of a parent region with all locations associated with it.

    Args:
        - parent_id (int): The unique identifier of the parent region.
        - db (Session): The database session to perform the query.

    Returns:
        - List[ParentLocationResponse]: A list of Region objects that are children of
        the specified parent region formated as a response object.
    """
    child_regions = get_regions(parent_id, db)

    response = [
        ParentLocationResponse(
            id=child.id,
            name=child.name,
            type=1,
            locations=get_child_locations(child.id, db),
        )
        for child in child_regions
    ]

    # Sorting result list
    sort_model = [{"sort": "asc", "colId": "name"}]
    result_list = sort_response(response, sort_model)

    return result_list


def get_child_locations(region_id, db: Session) -> List[LocationResponse]:
    """
    Get all children locations of a given region as a location response.

    Args:
        - region_id (int): The unique identifier of the parent region.
        - db (Session): The database session to perform the query.

    Returns:
        - List[LocationResponse]: A list of Location response objects that are
        children of the specified location.
    """
    locations = get_locations(region_id, db)

    response = [
        LocationResponse(id=location.id, name=location.name, type=2)
        for location in locations
    ]

    # Sorting result list
    sort_model = [{"sort": "asc", "colId": "name"}]
    result_list = sort_response(response, sort_model)

    return result_list


def is_integer(value):
    """
    Check if the given value can be converted to an integer.

    Args:
        value: The value to check.

    Returns:
        bool: True if the value can be converted to an integer, False otherwise.
    """
    try:
        int(value)
        return True
    except ValueError:
        return False


def get_lsa_lcns(asset_id, assets, db: Session = Depends(get_db)):
    """
    Get LSA LCN details for a given asset_id.
    """
    if is_integer(asset_id):
        cmms_asset = get_asset(asset_id, db)
        if cmms_asset.part_number is None:
            return []

        lsa_items = get_lcn_children(part_number=cmms_asset.part_number, db=db)
    else:
        lsa_items = get_lcn_children(lcn=asset_id, db=db)

    if lsa_items is None:
        return []

    # Filter out lsa_items that are already present in the list of assets
    for asset in assets:
        lsa_items = [
            lsa_item
            for lsa_item in lsa_items
            if lsa_item.part_number != asset.part_number
        ]

    result = []
    for lsa_item in lsa_items:
        asset_response = AssetResponse(
            id=lsa_item.lcn,
            name=lsa_item.name,
            part_number=lsa_item.part_number,
            version=None,
            serial_number=None,
            topological_name=None,
            type=2,
            status="Missing",
            has_children=lsa_item.has_children,
        )
        result.append(asset_response)

    return result
