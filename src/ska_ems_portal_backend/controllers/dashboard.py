"""
API endpoints for retrieving dashboard and widget data.
"""

from typing import Optional

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from ska_ems_portal_backend.db.session import get_db
from ska_ems_portal_backend.models.response.dashboard_response import (
    DashboardResponse,
    DashboardWidgetResponse,
)
from ska_ems_portal_backend.services.dashboard_service import (
    build_dashboard_response,
    build_widget_data_response,
    fetch_dashboard_by_id,
    fetch_global_dashboard,
    fetch_widget_by_id,
)
from ska_ems_portal_backend.utils.auth import validate_token
from ska_ems_portal_backend.utils.exception_handler import handle_request_exceptions

router = APIRouter()


@router.get("/dashboards", tags=["Dashboards"], response_model=DashboardResponse)
@handle_request_exceptions
async def get_dashboard(
    dashboard_id: Optional[int] = None,
    db: Session = Depends(get_db),
    _: None = Depends(validate_token),
):
    """
    Retrieve dashboard information.

    If `dashboard_id` is not provided, this endpoint returns the global dashboard,
    if available.
    """
    if dashboard_id is None:
        dashboard = fetch_global_dashboard(db)
    else:
        dashboard = fetch_dashboard_by_id(dashboard_id, db)
    if not dashboard:
        raise HTTPException(status_code=404, detail="Dashboard not found.")
    return build_dashboard_response(dashboard, db)


@router.get(
    "/dashboard_widgets/{widget_id}",
    tags=["Dashboards"],
    response_model=DashboardWidgetResponse,
)
async def get_widget_data_endpoint(
    widget_id: int, db: Session = Depends(get_db), _: None = Depends(validate_token)
):
    """
    Retrieve data for a specific widget by its widget ID.
    """
    widget = fetch_widget_by_id(widget_id, db)
    if not widget:
        raise HTTPException(status_code=404, detail="Widget not found")

    return build_widget_data_response(widget, db)
