"""
API endpoints for retrieving Work Order information.
"""

from typing import List, Optional

from fastapi import APIRouter, Depends, Query
from sqlalchemy.orm import Session

from ska_ems_portal_backend.db.session import get_db
from ska_ems_portal_backend.models.response.work_order_response import (
    CalendarResponseModel,
)
from ska_ems_portal_backend.services.cmms_service import get_tasks_within_date_range
from ska_ems_portal_backend.utils.auth import validate_token
from ska_ems_portal_backend.utils.date_utils import parse_iso_datetime
from ska_ems_portal_backend.utils.exception_handler import handle_request_exceptions

router = APIRouter()


@router.get(
    "/tasks",
    tags=["Work Orders"],
    response_model=List[CalendarResponseModel],
)
@handle_request_exceptions
async def get_tasks(
    start: str,
    end: str,
    status: Optional[str] = Query(None),
    priority: Optional[List[str]] = Query(None),
    db: Session = Depends(get_db),
    _: None = Depends(validate_token),
):
    """
    Retrieve all work orders events within a specific date range and also filter for
    status and priority.
    """
    start_date = parse_iso_datetime(start)
    end_date = parse_iso_datetime(end)

    tasks = get_tasks_within_date_range(db, start_date, end_date, status, priority)
    return tasks
