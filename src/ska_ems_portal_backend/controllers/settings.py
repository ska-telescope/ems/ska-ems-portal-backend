"""
API endpoints for user settings operations.
"""

from datetime import datetime
from typing import List

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from ska_ems_portal_backend.db.session import get_db
from ska_ems_portal_backend.models.api_gateway import Token
from ska_ems_portal_backend.models.response.api_token_response import (
    TokenCreateRequest,
    TokenInfoResponse,
    TokenResponse,
)
from ska_ems_portal_backend.utils.auth import validate_token
from ska_ems_portal_backend.utils.date_utils import calculate_expiration, to_epoch
from ska_ems_portal_backend.utils.exception_handler import handle_request_exceptions
from ska_ems_portal_backend.utils.token_utils import bigint_to_string, create_token

router = APIRouter()


@router.post(
    "/generate-api-gateway-token", tags=["Settings"], response_model=TokenResponse
)
@handle_request_exceptions
async def generate_api_gateway_token(
    token_data: TokenCreateRequest,
    db: Session = Depends(get_db),
    _: None = Depends(validate_token),
):
    """
    Generate an API Gateway token for the user to access the API Gateway.
    """

    new_token = create_token()

    expiration_time = calculate_expiration(token_data.expiration)

    token_record = Token(
        client_id=token_data.client_id,
        token=new_token,
        expires_at=to_epoch(expiration_time),
        scope=token_data.scope,
        description=token_data.description,
        creation_date=to_epoch(datetime.now()),
        last_used=None,
    )

    db.add(token_record)
    db.commit()
    db.refresh(token_record)

    return {
        "id": token_record.id,
        "client_id": token_record.client_id,
        "token": token_record.token,
        "expires_at": bigint_to_string(token_record.expires_at),
        "scope": token_record.scope.value,
        "description": token_record.description,
        "creation_date": bigint_to_string(token_record.creation_date),
        "last_used": bigint_to_string(token_record.last_used),
    }


@router.get(
    "/tokens/{user_id}", response_model=List[TokenInfoResponse], tags=["Settings"]
)
def get_tokens_for_user(
    user_id: str, db: Session = Depends(get_db), _: None = Depends(validate_token)
):
    """
    Get all the tokens for a specific user.
    """
    tokens = db.query(Token).filter(Token.client_id == user_id).all()

    if not tokens:
        return []
    return [
        TokenInfoResponse(
            id=token.id,
            expires_at=bigint_to_string(token.expires_at),
            scope=token.scope.value,
            description=token.description,
            creation_date=bigint_to_string(token.creation_date),
            last_used=bigint_to_string(token.last_used),
        )
        for token in tokens
    ]


@router.delete("/delete-token/{token_id}", tags=["Settings"])
async def delete_token(
    token_id: int, db: Session = Depends(get_db), _: None = Depends(validate_token)
):
    """
    Delete an API Gateway token by its ID.
    """

    token = db.query(Token).filter(Token.id == token_id).delete()

    if token == 0:
        raise HTTPException(status_code=404, detail="Token not found")
    db.commit()

    return {"detail": f"Token with ID {token_id} has been deleted successfully."}
