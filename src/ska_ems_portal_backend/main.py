"""
Main for the EMS Backend
"""

import logging
import os
from typing import Annotated, List

from fastapi import Depends, FastAPI, Query
from fastapi.middleware.cors import CORSMiddleware
from sqlalchemy.orm import Session

from ska_ems_portal_backend.controllers import (
    assets,
    dashboard,
    location,
    settings,
    tasks,
)
from ska_ems_portal_backend.db.session import get_db
from ska_ems_portal_backend.models.response.item_response import ItemResponse
from ska_ems_portal_backend.services.configuration_control_service import find_item
from ska_ems_portal_backend.utils.auth import validate_token

logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)

NAMESPACE_PREFIX = os.getenv("NAMESPACE_PREFIX", default="ska-ems-portal-backend")

app = FastAPI(
    title="Engineering Management System (EMS) Portal Backend",
    summary="""
    This API is used to provide access to Engineering
    Management System (EMS) portal
    """,
    docs_url=f"/{NAMESPACE_PREFIX}/docs",
    openapi_url=f"/{NAMESPACE_PREFIX}/openapi.json",
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        "https://ems.internal.skao.int",
        "http://localhost",
        "http://localhost:5173",
        "http://127.0.0.1",
        "http://127.0.0.1:5173",
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(assets.router)
app.include_router(location.router)
app.include_router(dashboard.router)
app.include_router(settings.router)
app.include_router(tasks.router)


@app.get("/")
async def root():
    """Unit test for the root path "/" """
    return {"message": "Hello World"}


@app.get("/search", response_model=List[ItemResponse])
async def quick_search(
    text: Annotated[str, Query(min_length=3)], db: Session = Depends(get_db)
):
    """
    Search for items by part number or description.
    """
    items = find_item(text, db)
    return items


@app.get("/test")
async def test():
    """Unit test for the root path "/" """
    return {"message": "Test"}


@app.get("/db")
async def test_db():
    """Unit test for the root path "/" """
    db = os.getenv("DATABASE_URL", default="No Env")
    return {"message": db}


@app.get("/me/groups", tags=["Me"])
def get_user_groups(token_payload: dict = Depends(validate_token)):
    """Retrieves the user's groups.

    Args:
        token (str): The authentication token.

    Returns:
        List[str]: A list of user groups on success.
    """
    return token_payload.get("groups", [])
